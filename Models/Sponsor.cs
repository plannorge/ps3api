namespace Ps3Api.Models
{
    public class Sponsor
    {
        public string forename {get; set;}
        public string surname {get; set;}
        public string street { get; set;}
        public string houseNumber { get; set;}
        public string appartmentNumber { get; set;}
        public string postCode { get; set;}
        public string town{ get; set;}
        public string phoneNumber { get; set;}
        public string eMailAddress { get; set;}
        public string countryCode { get; set;}
        public string cByO { get; set;}
        public System.DateTime? dateOfBirth { get; set;}
        public string kid { get; set;}
        public string ssn { get; set;}
        public string sourceCode { get; set;}
        public System.Collections.Generic.List<Preference> preferences {get;set;}

        public Sponsor(
            string forename,
            string surname,
            string street,
            string houseNumber,
            string appartmentNumber,
            string postCode,
            string town,
            string phoneNumber,
            string eMailAddress,
            string countryCode,
            string cByO,
            System.DateTime? dateOfBirth,
            string kid,
            string ssn,
            string sourceCode)
        {
            this.forename = forename;
            this.surname = surname;
            this.street = street;
            this.houseNumber = houseNumber;
            this.appartmentNumber = appartmentNumber;
            this.postCode = postCode;
            this.town = town;
            this.phoneNumber = phoneNumber;
            this.eMailAddress = eMailAddress;
            this.countryCode = countryCode;
            this.cByO = cByO;
            this.dateOfBirth = dateOfBirth;
            this.kid = kid;
            this.ssn = ssn;
            this.sourceCode = sourceCode;
        }
    }
}