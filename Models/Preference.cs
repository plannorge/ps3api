namespace Ps3Api.Models
{
    public class Preference
    {
        public string gender {get; set;}
        public int? ageFrom {get; set;}
        public int? ageTo { get; set;}
        public string countryCode { get; set;}
        public string continentCode { get; set;}
        public int? amount { get; set;}
        
        public Preference(
            string gender,
            int? ageFrom,
            int? ageTo,
            string countryCode,
            string continentCode,
            int? amount)
        {
			this.gender = gender;
			this.ageFrom = ageFrom;
			this.ageTo = ageTo;
			this.countryCode = countryCode;
			this.continentCode = continentCode;
			this.amount = amount;
        }
	}
}
