namespace Ps3Api.Models
{
    // I think we are only allowed to submit datatypes that has a defined model, ie we are not allowed to submit a comibation of datatypes. Hence I had to create this container class. I do not like it.
    public class PreferenceWithSpNumber
    {
        public System.Int64 spNumber {get; set;}
        public Ps3Api.Models.Preference preference {get; set;}

        public PreferenceWithSpNumber(
            System.Int64 spNumber,
            Ps3Api.Models.Preference preference)
        {
			this.spNumber = spNumber;
			this.preference = preference;
        }
	}
}
