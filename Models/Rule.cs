namespace Ps3Api.Models
{
    public class Rule
    {
        public string type {get; set;}
        public int? minLength {get; set;} // For strings only
        public int? maxLength {get; set;} // For strings only
        public string validCharacters {get; set;} // For strings only
        public bool allowEmpty {get; set;}
        public int? minimum {get; set;} // For numeric types only
        public int? maximum {get; set;} // For numeric types only
        public string greaterThan {get; set;} // For numeric types only, contains the variable name of the variable to compare with
        public string lessThan {get; set;} // For numeric types only, contains the variable name of the variable to compare with
        public string greaterThanOrEqualTo {get; set;} // For numeric types only, contains the variable name of the variable to compare with
        public string lessThanOrEqualTo {get; set;} // For numeric types only, contains the variable name of the variable to compare with
        public int? maxYearDifference {get; set;} // For DateTime only
        public int? minYearDifference {get; set;} // For DateTime only

        // TODO: Add more criterias if/when needed
	}
}
