namespace Ps3Api.Models
{
    public class DualSponsor
    {
        public Sponsor primarySponsor {get; set;}
        public Sponsor secondarySponsor {get; set;}
        public System.Collections.Generic.List<Preference> preferences {get;set;}

        public DualSponsor(
            Sponsor primarySponsor,
            Sponsor secondarySponsor,
            System.Collections.Generic.List<Preference> preferences)
        {
            this.primarySponsor = primarySponsor;
            this.secondarySponsor = secondarySponsor;
            this.preferences = preferences;
        }
    }
}