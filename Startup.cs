﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;
using Ps3Api.Models;

namespace Ps3Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
			services.Configure<IConfiguration>(Configuration);

            // https://auth0.com/blog/securing-asp-dot-net-core-2-applications-with-jwts/
            services.AddAuthentication(Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(
                    options => {
                        options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters{
                            ValidateIssuer = true,
                            ValidateAudience = true,
                            ValidateLifetime = true,
                            ValidateIssuerSigningKey = true,
                            ValidIssuer = Configuration["Jwt:Issuer"],
                            ValidAudience = Configuration["Jwt:Issuer"],
                            IssuerSigningKey = new Microsoft.IdentityModel.Tokens.SymmetricSecurityKey(System.Text.Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]))
                        };
                    }
                );

            services.AddMvc().SetCompatibilityVersion(Microsoft.AspNetCore.Mvc.CompatibilityVersion.Version_2_1);

            services.AddSwaggerGen(
                c => {
                    c.SwaggerDoc(
                        "v1", new Swashbuckle.AspNetCore.Swagger.Info {
                            Title = "Ps3 Signup API",
                            Version ="v1",
                            Description = "A proof of concept API. The goal is to provide a performance/use-case focused API. This API has not been developed or approved by the PlanSYS Consortium, hence it may perform database insert/updates/deletes that breaks PlanSYS logic.",
                            Contact = new Swashbuckle.AspNetCore.Swagger.Contact{
                                Name = "Magnus Oen Pedersen",
                                Email = "magnusop@plan-norge.no"
                            },
                            License = new Swashbuckle.AspNetCore.Swagger.License{
                                Name = "GNU General Public License v3.0",
                                Url = "https://www.gnu.org/licenses/gpl-3.0.en.html"
                            }
                        }
                    );

                    c.AddSecurityDefinition("Bearer", new Swashbuckle.AspNetCore.Swagger.ApiKeyScheme
                        {
                            Description = "JWT Authorization header using the Bearer scheme. Example\"Authorization: Bearer {token}\"",
                            Name = "Authorization",
                            In = "header",
                            Type = "apiKey"
                        }
                    );

                    Dictionary<string, IEnumerable<string>> security = new Dictionary<string, IEnumerable<string>>{
                        {"Bearer", new string[] {}}
                    };

                    c.AddSecurityRequirement(security);
                }
            );
            //services.AddMvc();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }
			
            app.UseHttpsRedirection();

            app.UseAuthentication();

            app.UseSwagger(
                c => {
                    c.PreSerializeFilters.Add((swagger, httpReq) => swagger.Host = httpReq.Host.Value);
                }

            );

            app.UseSwaggerUI(
                c => {
                    c.RoutePrefix = String.Empty;
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Ps3Api v1");
                    c.ShowExtensions();
                }
            );

            app.UseMvc();

        }
    }
}
