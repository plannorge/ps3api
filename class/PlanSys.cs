using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authorization;
using Ps3Api.Models;

namespace Ps3Api.Misc{

    public class PlanSys{
        private IConfiguration configuration;
        private System.Data.SqlClient.SqlTransaction transaction;
        private System.Data.SqlClient.SqlConnection connection;

        public PlanSys(IConfiguration configuration) {
            this.configuration = configuration;
        }

        public Sponsor GetExternalRelation(Int64 externalRelationNumber)
        {

            Sponsor sponsor = null;

            using (this.connection = new SqlConnection(this.configuration.GetConnectionString("DefaultConnection")))
			{
                this.connection.Open();

                string sql = "SELECT ExternalRelationAddress.ExternalRelationNumber AS SPNumber, ExternalRelationAddress.Name1TitleCode AS TitleCode, ExternalRelationAddress.Name1Line1 AS Forename, ExternalRelationAddress.Name1Line2 AS Surname, ExternalRelationAddress.Street AS Street, ExternalRelationAddress.HouseNumber AS HouseNumber, ExternalRelationAddress.AppartmentNumber AS AppartmentNumber, ExternalRelationAddress.PostCode AS PostCode, ExternalRelationAddress.Town AS Town, ExternalRelationAddress.CountryCode AS CountryCode, ExternalRelationAddress.CByO AS CByO, ExternalRelationAddress.Mobile AS PhoneNumber, ExternalRelationAddress.PrimaryEMail AS EMailAddress, Activity.SourceCode AS SourceCode, KID.KidNumber AS KID, PersonalId.PersonalId AS SSN, PersonalId.Dob AS DateOfBirth FROM (SELECT Name1TitleCode, ExternalRelationNumber, Name1Line1, Name1Line2, Street, HouseNumber, AppartmentNumber, PostCode, Town, CountryCode, CByO, TelephonePrivate, TelephoneBusiness, Mobile, PrimaryEMail, AddressId FROM ExternalRelationAddress WHERE AddressTypeCode = 'N' AND IsActive = 'Y') ExternalRelationAddress INNER JOIN (SELECT ExternalRelationNumber, ActivityID, SourceCode FROM Activity WHERE ActivityTypeCode IN ('SP', 'CT')) AS Activity ON ExternalrelationAddress.ExternalRelationNumber = Activity.ExternalRelationNumber LEFT JOIN (SELECT ExternalRelationNumber, PaymentMethodID, PaymentTypeCode FROM ExternalRelationPaymentMethod WHERE PaymentMethodID IN (SELECT PaymentMethodID FROM Commitment WHERE IsActive='Y')) ExternalRelationPaymentMethod ON ExternalRelationAddress.ExternalRelationNumber = ExternalRelationPaymentMethod.ExternalRelationNumber LEFT JOIN KIDPaymentMethod ON ExternalRelationPaymentMethod.PaymentMethodID = KIDPaymentMethod.PaymentMethodID LEFT JOIN KID ON KIDPaymentMethod.KidID = KID.KidID LEFT JOIN ExternalRelationAddressPersonalId ON ExternalRelationAddress.AddressID = ExternalRelationAddressPersonalId.AddressID LEFT JOIN PersonalId ON ExternalRelationAddressPersonalId.Pid = PersonalId.Pid WHERE ExternalRelationAddress.ExternalRelationNumber = @ExternalRelationNumber;";

                SqlDataReader reader;
                
                using (SqlCommand command = new SqlCommand(sql, this.connection))
                {
                    command.Parameters.AddWithValue("@ExternalRelationNumber", externalRelationNumber);
                        
                    reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            sponsor = new Sponsor(
                                System.DBNull.Value != reader["Forename"] ? reader["Forename"].ToString().Trim() : null,
                                System.DBNull.Value != reader["Surname"] ? reader["Surname"].ToString().Trim() : null,
                                System.DBNull.Value != reader["Street"] ? reader["Street"].ToString().Trim() : null,
                                System.DBNull.Value != reader["HouseNumber"] ? reader["HouseNumber"].ToString().Trim() : null,
                                System.DBNull.Value != reader["AppartmentNumber"] ? reader["AppartmentNumber"].ToString().Trim() : null,
                                System.DBNull.Value != reader["PostCode"] ? reader["PostCode"].ToString().Trim() : null,
								System.DBNull.Value != reader["Town"] ? reader["Town"].ToString().Trim() : null,
                                System.DBNull.Value != reader["PhoneNumber"] ? reader["PhoneNumber"].ToString().Trim() : null,
                                System.DBNull.Value != reader["EMailAddress"] ? reader["EMailAddress"].ToString().Trim() : null,
                                System.DBNull.Value != reader["CountryCode"] ? reader["CountryCode"].ToString().Trim() : null,
                                System.DBNull.Value != reader["CByO"] ? reader["CByO"].ToString().Trim() : null,
                                System.DBNull.Value != reader["DateOfBirth"] ? DateTime.Parse(reader["DateOfBirth"].ToString()) : (DateTime?)null,
                                System.DBNull.Value != reader["KID"] ? reader["KID"].ToString().Trim() : null,
                                System.DBNull.Value != reader["SSN"] ? reader["SSN"].ToString().Trim() : null,
                                System.DBNull.Value != reader["SourceCode"] ? reader["SourceCode"].ToString().Trim() : null
                            );
                        }
                    }
                    reader.Close();
                }

                sql = "SELECT Preference.ContinentCode, Preference.Sex AS Gender, Preference.AgeFrom, Preference.AgeTo, PreferenceCountry.CountryCode, Preference.Amount FROM Activity JOIN Preference ON Activity.ActivityID = Preference.ActivityId LEFT JOIN PreferenceCountry ON Preference.PreferenceID = PreferenceCountry.PreferenceID WHERE Activity.ExternalRelationNumber = @ExternalRelationNumber AND (PreferenceCountry.PreferenceSequence IS NULL OR PreferenceCountry.PreferenceSequence = 1);";

                using (SqlCommand command = new SqlCommand(sql, this.connection))
                {
                    command.Parameters.AddWithValue("@ExternalRelationNumber", externalRelationNumber);
                        
                    reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        sponsor.preferences = new System.Collections.Generic.List<Preference>();
                        Preference preference;
                        while (reader.Read())
                        {
                            preference = new Preference(
                                System.DBNull.Value != reader["Gender"] ? reader["Gender"].ToString() : null,
                                System.DBNull.Value != reader["AgeFrom"] ? Int32.Parse(reader["AgeFrom"].ToString()) : (int?) null,
                                System.DBNull.Value != reader["AgeTo"] ? Int32.Parse(reader["AgeTo"].ToString()) : (int?) null,
                                System.DBNull.Value != reader["CountryCode"] ? reader["CountryCode"].ToString() : null,
                                System.DBNull.Value != reader["ContinentCode"] ? reader["ContinentCode"].ToString() : null,
                                System.DBNull.Value != reader["Amount"] ? Decimal.ToInt32(Decimal.Parse(reader["Amount"].ToString())) : (int?)null
                            );

                            sponsor.preferences.Add(preference);
                        }
                    }
                    reader.Close();
                }

                

                this.connection.Close();
            }

            return sponsor;
        }

        public Preference GetPreference(Int64 preferenceId)
        {
            Preference preference = null;

            using (this.connection = new SqlConnection(this.configuration.GetConnectionString("DefaultConnection")))
			{
                this.connection.Open();

                String sql = "SELECT Preference.ContinentCode, Preference.Sex AS Gender, Preference.AgeFrom, Preference.AgeTo, PreferenceCountry.CountryCode, Preference.Amount FROM Preference LEFT JOIN PreferenceCountry ON Preference.PreferenceID = PreferenceCountry.PreferenceID WHERE Preference.PreferenceId = @PreferenceId AND (PreferenceCountry.PreferenceSequence IS NULL OR PreferenceCountry.PreferenceSequence = 1);";

                SqlDataReader reader;

                using (SqlCommand command = new SqlCommand(sql, this.connection))
                {
                    command.Parameters.AddWithValue("@PreferenceId", preferenceId);
                        
                    reader = command.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            preference = new Preference(
                                System.DBNull.Value != reader["Gender"] ? reader["Gender"].ToString() : null,
                                System.DBNull.Value != reader["AgeFrom"] ? Int32.Parse(reader["AgeFrom"].ToString()) : (int?) null,
                                System.DBNull.Value != reader["AgeTo"] ? Int32.Parse(reader["AgeTo"].ToString()) : (int?) null,
                                System.DBNull.Value != reader["CountryCode"] ? reader["CountryCode"].ToString() : null,
                                System.DBNull.Value != reader["ContinentCode"] ? reader["ContinentCode"].ToString() : null,
                                System.DBNull.Value != reader["Amount"] ? Decimal.ToInt32(Decimal.Parse(reader["Amount"].ToString())) : (int?)null
                            );
                        }
                    }
                    reader.Close();
                }

                this.connection.Close();
            }

            return preference;
        }

        public bool HasActivity(Int64 externalRealtionNumber, string activityTypeCode)
        {
            String sql = "SELECT ExternalRelationNumber FROM Activity WHERE ActivityTypeCode = @ActivityTypeCode AND ExternalRelationNumber = @ExternalRelationNumber AND IsActive = 'Y';";

            SqlDataReader reader;

            using (SqlCommand command = new SqlCommand(sql, this.connection))
            {
                command.Transaction = this.transaction;

                command.Parameters.AddWithValue("@ActivityTypeCode", activityTypeCode);
                command.Parameters.AddWithValue("@ExternalRelationNumber", externalRealtionNumber);
                    
                reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        // This if statement is sort of not neccessary. The check has already been done in SQL.
                        if (Int64.Parse(reader["ExternalRelationNumber"].ToString()) == externalRealtionNumber)
                        {
                            return true;
                        }
                    }
                }
                reader.Close();
            }

            return false;
        }

        private Int64 GetPaymentMethod(Int64 externalRealtionNumber)
        {
            // Query is slightly more complicated than it needs to be. I am trying to re-use paymentmethods that already are connected to a SP activity.
            String sql = "SELECT ExternalRelationPaymentMethod.PaymentMethodId FROM	ExternalRelationPaymentMethod LEFT JOIN	Activity ON ExternalRelationPaymentMethod.PaymentMethodID = Activity.PaymentMethodId WHERE ExternalRelationPaymentMethod.ExternalRelationNumber = @ExternalRelationNumber AND (Activity.ActivityTypeCode IS NULL OR Activity.ActivityTypeCode = 'SP') ORDER BY Activity.PaymentMethodId DESC";

            SqlDataReader reader;

            Int64 result = -1;

            using (SqlCommand command = new SqlCommand(sql, this.connection))
            {
                command.Transaction = this.transaction;

                command.Parameters.AddWithValue("@ExternalRelationNumber", externalRealtionNumber);
                    
                reader = command.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        result = Int64.Parse(reader["PaymentMethodId"].ToString());
                        break;
                    }
                }
                reader.Close();
            }

            if (result > -1)
            {
                return result;
            }
            // Did not find a paymentmethod
            throw new Exception();
        }

        public Dictionary<Int64, string> GetDuplicates(Sponsor sponsor)
        {
            // Testing 3 criterieas:
            // 1) Equal Forename, Surname, PostCode
            // 2) Equal PhoneNumber
            // 3) Equal EMailAddress

            Dictionary<Int64, string> duplicates = new Dictionary<Int64, string>();

            List<string> duplicateReasons = null;

            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            
            using (this.connection = new SqlConnection(this.configuration.GetConnectionString("DefaultConnection")))
            {
                this.connection.Open();
                String sql = "SELECT ExternalRelationNumber, Name1Line1, Name1Line2, PostCode, TelephonePrivate, TelephoneBusiness, Mobile, PrimaryEMail FROM ExternalRelationAddress WHERE (";

                using (SqlCommand command = new SqlCommand())
                {
                    SqlDataReader reader = null;;

                    if (!String.IsNullOrEmpty(sponsor.forename) &&
                        !String.IsNullOrEmpty(sponsor.surname) &&
                        !String.IsNullOrEmpty(sponsor.postCode))
                    {

                        sql += " (LOWER(Name1Line1)=LOWER(@Forename) AND LOWER(Name1Line2)=LOWER(@Surname) AND LOWER(PostCode)=LOWER(@PostCode))";

                        command.Parameters.AddWithValue("@Forename", sponsor.forename);
                        command.Parameters.AddWithValue("@Surname", sponsor.surname);
                        command.Parameters.AddWithValue("@PostCode", sponsor.postCode);
                    }

                    if (!String.IsNullOrEmpty(sponsor.phoneNumber))
                    {
                        sql += " OR (TelephonePrivate=@PhoneNumber OR TelephoneBusiness=@PhoneNumber OR Mobile=@PhoneNumber)";
                        command.Parameters.AddWithValue("@PhoneNumber", sponsor.phoneNumber);
                    }

                    if (!String.IsNullOrEmpty(sponsor.eMailAddress))
                    {
                        sql += " OR (LOWER(PrimaryEMail)=LOWER(@EMailAddress))";
                        command.Parameters.AddWithValue("@EMailAddress", sponsor.eMailAddress);
                    }

                    sql += ") AND AddressTypeCode = 'N' AND IsActive = 'Y'";

                    command.CommandText = sql;
                    command.Connection = this.connection;

                    reader = command.ExecuteReader();
                    sw.Stop();
                    Console.WriteLine("GetDuplicates() - Ticks: " + sw.ElapsedTicks + " ms: " + sw.ElapsedMilliseconds);

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            duplicateReasons = new List<string>();

                            if (!duplicates.ContainsKey((Convert.ToInt64(reader["ExternalRelationNumber"]))))
                            {
                                if (reader["Name1Line1"].ToString() == sponsor.forename &&
                                    reader["Name1Line2"].ToString() == sponsor.surname &&
                                    reader["PostCode"].ToString() == sponsor.postCode)
                                {
                                    duplicateReasons.Add("(forename, surname and postCode)");
                                }

                                if (reader["PrimaryEMail"].ToString() == sponsor.eMailAddress)
                                {
                                    duplicateReasons.Add("(eMailAddress)");
                                }

                                if (reader["TelephonePrivate"].ToString() == sponsor.phoneNumber ||
                                    reader["TelephoneBusiness"].ToString() == sponsor.phoneNumber ||
                                    reader["Mobile"].ToString() == sponsor.phoneNumber)
                                {
                                        duplicateReasons.Add("(phoneNumber)");
                                }

                                duplicates.Add(Convert.ToInt64(reader["ExternalRelationNumber"]), string.Join(", ", duplicateReasons.ToArray()));
                            }
                        }
                    }

                    reader.Close();

                    this.connection.Close();
                }
            }

            return duplicates;
        }

        public void LinkExternalRelations(Int64 externalRelationNumberPrimary, Int64 externalRelationNumberSecondary)
        {
            // Profiling manually
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();

            sw.Reset();
            sw.Start();

            DateTime now = DateTime.Now;

            string sql = "INSERT INTO ExternalRelationLink (LinkCode, LinkNODB, ExternalRelationNumber, ExternalRelationNODB, LinkWithExternalRelationNumber, LinkWithExternalRelationNODB, LinkStartDate, IsActive, CreatedByUserName, CreatedByNODB, CreatedOn, LastUpdatedByUserName, LastUpdatedByNODB, LastUpdatedOn) VALUES (@LinkCode, @LinkNODB, @ExternalRelationNumber, @ExternalRelationNODB, @LinkWithExternalRelationNumber, @LinkWithExternalRelationNODB, @LinkStartDate, @IsActive, @CreatedByUserName, @CreatedByNODB, @CreatedOn, @LastUpdatedByUserName, @LastUpdatedByNODB, @LastUpdatedOn)";

            using (this.connection = new System.Data.SqlClient.SqlConnection(this.configuration.GetConnectionString("DefaultConnection")))
            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(sql, this.connection))
            {
                this.connection.Open();

                command.Parameters.AddWithValue("@LinkCode", "S2");
                command.Parameters.AddWithValue("@LinkNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@ExternalRelationNumber", externalRelationNumberPrimary);
                command.Parameters.AddWithValue("@ExternalRelationNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@LinkWithExternalRelationNumber", externalRelationNumberSecondary);
                command.Parameters.AddWithValue("@LinkWithExternalRelationNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@LinkStartDate", now);
                command.Parameters.AddWithValue("@IsActive", "Y");
                command.Parameters.AddWithValue("@CreatedByUsername", this.configuration["PlanSys:Username"]);
                command.Parameters.AddWithValue("@CreatedByNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@CreatedOn", now);
                command.Parameters.AddWithValue("@LastUpdatedByUsername", this.configuration["PlanSys:Username"]);
                command.Parameters.AddWithValue("@LastUpdatedByNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@LastUpdatedOn", now);

                try
                {
                    command.ExecuteNonQuery();
                }
				catch (Exception exception)
				{
					this.transaction.Rollback();
					Console.WriteLine(exception.Message);
                    throw exception;
				}

                sw.Stop();
                Console.WriteLine("CreateExternalRelation() - Ticks: " + sw.ElapsedTicks + " ms: " + sw.ElapsedMilliseconds);
            }
            return;
        }

        public Int64 CreatePreference(Sponsor sponsor, Int64 externalRelationNumber, Preference preference)
        {
            DateTime now = DateTime.Now;

            Int64 activityId = -1;
            Int64 paymentMethodId = -1;
            Int64 activityExtensionId = -1;
            Int64 commitmentId = -1;
            Int64 commitmentExtensionId = -1;
            Int64 preferenceExtensionId = -1;
            Int64 preferenceId = -1;

            using (this.connection = new System.Data.SqlClient.SqlConnection(this.configuration.GetConnectionString("DefaultConnection")))
            {
                this.connection.Open();
                this.transaction = this.connection.BeginTransaction(System.Data.IsolationLevel.Serializable, "CreatePreference");

                try
                {
                    paymentMethodId = this.GetPaymentMethod(externalRelationNumber);
                }
                catch(Exception exception)
                {
                    Console.WriteLine("No existing paymentmethod, creating a new one: " + exception.Message);
                    paymentMethodId = this.CreateExternalRelationPaymentMethod(sponsor, externalRelationNumber, now);
                }

                if (!this.HasActivity(externalRelationNumber, "SP"))
                {
                    try
                    {
                        activityId = this.CreateActivity(sponsor, externalRelationNumber, paymentMethodId, now);
                    }
                    catch (Exception exception)
                    {
                            this.transaction.Rollback();
                            Console.WriteLine(exception.Message);
                            throw exception;
                    }

                    try
                    {
                        activityExtensionId = this.CreateActivity_ExtensionPS3(sponsor, activityId, now);
                    }
                    catch (Exception exception)
                    {
                            this.transaction.Rollback();
                            Console.WriteLine(exception.Message);
                            throw exception;
                    }
                }

                try
                {
                    commitmentId = this.CreateCommitment(sponsor, externalRelationNumber, paymentMethodId, now);
                }
                catch (Exception exception)
                {
                    this.transaction.Rollback();
                    Console.WriteLine(exception.Message);
                    throw exception;
                }

                try
                {
                    // This table creates no unique id
                    this.CreateCommitmentAmount(sponsor, commitmentId, preference, now);
                }
                catch (Exception exception)
                {
                    this.transaction.Rollback();
                    Console.WriteLine(exception.Message);
                    throw exception;
                }

                try
                {
                    commitmentExtensionId = this.CreateCommitment_ExtensionPS3(sponsor, externalRelationNumber, commitmentId, now);
                }
                catch (Exception exception)
                {
                    this.transaction.Rollback();
                    Console.WriteLine(exception.Message);
                    throw exception;
                }

                try
                {
                    preferenceId = this.CreatePreference(preference, sponsor, activityId, paymentMethodId, commitmentId, now);
                }
                catch (Exception exception)
                {
                    this.transaction.Rollback();
                    Console.WriteLine(exception.Message);
                    throw exception;
                }

                try
                {
                    preferenceExtensionId = this.CreatePreference_ExtensionPS3(preferenceId, externalRelationNumber, now);
                }
                catch (Exception exception)
                {
                    this.transaction.Rollback();
                    Console.WriteLine(exception.Message);
                    throw exception;
                }

                if (!String.IsNullOrEmpty(preference.countryCode))
                {
                    this.CreatePreferenceCountry(preference, preferenceId, now);
                }

                this.transaction.Commit();
                this.connection.Close();
            }

			Console.WriteLine("externalRelationNumber = " + externalRelationNumber);
			Console.WriteLine("paymentMethodId = " + paymentMethodId);
			Console.WriteLine("activityId = " + activityId);
			Console.WriteLine("commitmentIds = " + commitmentId);
			Console.WriteLine("commitmentExtensionIsd = " + commitmentExtensionId);
			Console.WriteLine("preferenceIds = " + preferenceId);
			Console.WriteLine("preferenceExtensionIds = " + preferenceId);

			if (preferenceId == -1)
			{
                throw new Exception("Something went wrong, not sure why. This error is triggered in the final check to make sure everything went okay. However, preferenceId is still -1 (default).");
			}

            return preferenceId;
        }

        public Int64 CreateExternalRelation(Sponsor sponsor)
        {
            // This function assumes that all variables in sponsor has been validated. No validation is performed in this function

            DateTime now = DateTime.Now;

            Int64 externalRelationNumber = -1;
            Int64 addressId = -1;
            Int64 pid = -1;
            Int64 externalRelationAdddressPersonalId = -1;
            Int64 activityId = -1;
            Int64 activityExtensionId = -1;
			Int64 commitmentId = -1;
			Int64 commitmentExtensionId = -1;
            Int64 paymentMethodId = -1;
            Int64 preferenceId = -1;
			Int64 preferenceExtensionId = -1;


			List <Int64> commitmentIds = new List<Int64>();
			List <Int64> commitmentExtensionIds = new List<Int64>();
			List <Int64> preferenceIds = new List<Int64>();
			List <Int64> preferenceExtensionIds = new List<Int64>();

			//Console.WriteLine(sponsor.ToString());

            // Profiling manually
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();

            using (this.connection = new System.Data.SqlClient.SqlConnection(this.configuration.GetConnectionString("DefaultConnection")))
            {
                this.connection.Open();
                this.transaction = this.connection.BeginTransaction(System.Data.IsolationLevel.Serializable, "CreateExternalRelation");

				try {
                    sw.Reset();
                    sw.Start();
					externalRelationNumber = this.CreateExternalRelation(now);
                    sw.Stop();
                    Console.WriteLine("CreateExternalRelation() - Ticks: " + sw.ElapsedTicks + " ms: " + sw.ElapsedMilliseconds);
				}
				catch (Exception exception)
				{
					this.transaction.Rollback();
					Console.WriteLine(exception.Message);
                    throw exception;
				}

 				try
                {
                    sw.Reset();
                    sw.Start();
                    addressId = this.CreateExternalRelationAddress(sponsor, externalRelationNumber, now);
                    sw.Stop();
                    Console.WriteLine("CreateExternalRelationAddress() - Ticks: " + sw.ElapsedTicks + " ms: " + sw.ElapsedMilliseconds);
                }
                catch (Exception exception)
                {
                    this.transaction.Rollback();
					Console.WriteLine(exception.Message);
                    throw exception;
                }


				if (!String.IsNullOrEmpty(this.configuration["PlanSys:DefaultSecondaryLanguage"]))
				{
					try
					{
                        sw.Reset();
                        sw.Start();
						this.CreateExternalRelationLanguage(externalRelationNumber, now);
                        sw.Stop();
                        Console.WriteLine("CreateExternalRelationLanguage() - Ticks: " + sw.ElapsedTicks + " ms: " + sw.ElapsedMilliseconds);
					}
					catch (Exception exception)
					{
						this.transaction.Rollback();
						Console.WriteLine(exception.Message);
                        throw exception;
					}
				}

                if (!String.IsNullOrEmpty(sponsor.ssn) || sponsor.dateOfBirth.HasValue)
                {
                    try
                    {
                        sw.Reset();
                        sw.Start();
                        pid = this.CreatePersonalId(sponsor, now);
                        sw.Stop();
                        Console.WriteLine("CreatePersonalId() - Ticks: " + sw.ElapsedTicks + " ms: " + sw.ElapsedMilliseconds);
                    }
                    catch (Exception exception)
                    {
						this.transaction.Rollback();
						Console.WriteLine(exception.Message);
                        throw exception;
                    }

                    try
                    {
                        sw.Reset();
                        sw.Start();
                        externalRelationAdddressPersonalId = this.CreateExternalRelationAddressPersonalId(addressId, pid, now);
                        sw.Stop();
                        Console.WriteLine("externalRelationAdddressPersonalId() - Ticks: " + sw.ElapsedTicks + " ms: " + sw.ElapsedMilliseconds);
                    }
                    catch (Exception exception)
                    {
						this.transaction.Rollback();
						Console.WriteLine(exception.Message);
                        throw exception;
                    }
                }

                try
                {
                    sw.Reset();
                    sw.Start();
                    paymentMethodId = this.CreateExternalRelationPaymentMethod(sponsor, externalRelationNumber, now);
                    sw.Stop();
                    Console.WriteLine("CreateExternalRelationPaymentMethod() - Ticks: " + sw.ElapsedTicks + " ms: " + sw.ElapsedMilliseconds);
                }
                catch (Exception exception)
                {
						this.transaction.Rollback();
						Console.WriteLine(exception.Message);
                        throw exception;
                }

                try
                {
                    sw.Reset();
                    sw.Start();
                    activityId = this.CreateActivity(sponsor, externalRelationNumber, paymentMethodId, now);
                    sw.Stop();
                    Console.WriteLine("CreateActivity() - Ticks: " + sw.ElapsedTicks + " ms: " + sw.ElapsedMilliseconds);
                }
                catch (Exception exception)
                {
						this.transaction.Rollback();
						Console.WriteLine(exception.Message);
                        throw exception;
                }

                try
                {
                    sw.Reset();
                    sw.Start();
                    activityExtensionId = this.CreateActivity_ExtensionPS3(sponsor, activityId, now);
                    sw.Stop();
                    Console.WriteLine("CreateActivity_ExtensionPS3() - Ticks: " + sw.ElapsedTicks + " ms: " + sw.ElapsedMilliseconds);
                }
                catch (Exception exception)
                {
						this.transaction.Rollback();
						Console.WriteLine(exception.Message);
                        throw exception;
                }

                foreach (Preference preference in sponsor.preferences)
                {
                    try
                    {
                        sw.Reset();
                        sw.Start();
                        commitmentId = this.CreateCommitment(sponsor, externalRelationNumber, paymentMethodId, now);
                        sw.Stop();
                        Console.WriteLine("CreateCommitment() - Ticks: " + sw.ElapsedTicks + " ms: " + sw.ElapsedMilliseconds);
                    }
                    catch (Exception exception)
                    {
                        this.transaction.Rollback();
                        Console.WriteLine(exception.Message);
                        throw exception;
                    }

                    try
                    {
                        // This table creates no unique id
                        sw.Reset();
                        sw.Start();
                        this.CreateCommitmentAmount(sponsor, commitmentId, preference, now);
                        sw.Stop();
                        Console.WriteLine("CreateCommitmentAmount() - Ticks: " + sw.ElapsedTicks + " ms: " + sw.ElapsedMilliseconds);
                    }
                    catch (Exception exception)
                    {
                        this.transaction.Rollback();
                        Console.WriteLine(exception.Message);
                        throw exception;
                    }

                    try
                    {
                        sw.Reset();
                        sw.Start();
                        commitmentExtensionId = this.CreateCommitment_ExtensionPS3(sponsor, externalRelationNumber, commitmentId, now);
                        sw.Stop();
                        Console.WriteLine("CreateCommitment_ExtensionPS3() - Ticks: " + sw.ElapsedTicks + " ms: " + sw.ElapsedMilliseconds);
                    }
                    catch (Exception exception)
                    {
                        this.transaction.Rollback();
                        Console.WriteLine(exception.Message);
                        throw exception;
                    }

					try
					{
                        sw.Reset();
                        sw.Start();
                    	preferenceId = this.CreatePreference(preference, sponsor, activityId, paymentMethodId, commitmentId, now);
                        sw.Stop();
                        Console.WriteLine("CreatePreference() - Ticks: " + sw.ElapsedTicks + " ms: " + sw.ElapsedMilliseconds);
					}
					catch (Exception exception)
					{
						this.transaction.Rollback();
						Console.WriteLine(exception.Message);
                        throw exception;
					}

					try
					{
                        sw.Reset();
                        sw.Start();
                    	preferenceExtensionId = this.CreatePreference_ExtensionPS3(preferenceId, externalRelationNumber, now);
                        sw.Stop();
                        Console.WriteLine("CreatePreference_ExtensionPS3() - Ticks: " + sw.ElapsedTicks + " ms: " + sw.ElapsedMilliseconds);
					}
					catch (Exception exception)
					{
						this.transaction.Rollback();
						Console.WriteLine(exception.Message);
                        throw exception;
					}

                    commitmentIds.Add(commitmentId);
                    commitmentExtensionIds.Add(commitmentExtensionId);
                    preferenceIds.Add(preferenceId);
					preferenceExtensionIds.Add(preferenceExtensionId);

                    if (!String.IsNullOrEmpty(preference.countryCode))
                    {
                        sw.Reset();
                        sw.Start();
                        this.CreatePreferenceCountry(preference, preferenceId, now);
                        sw.Stop();
                        Console.WriteLine("CreatePreferenceCountry() - Ticks: " + sw.ElapsedTicks + " ms: " + sw.ElapsedMilliseconds);
                    }
                }

                this.transaction.Commit();
                this.connection.Close();

			}

			Console.WriteLine("externalRelationNumber = " + externalRelationNumber);
			Console.WriteLine("addressId = " + addressId);
			Console.WriteLine("pid = " + pid);
			Console.WriteLine("externalRelationAdddressPersonalId = " + externalRelationAdddressPersonalId);
			Console.WriteLine("paymentMethodId = " + paymentMethodId);
			Console.WriteLine("activityId = " + activityId);
			Console.WriteLine("commitmentIds = " + string.Join(", ", commitmentIds));
			Console.WriteLine("commitmentExtensionIsd = " + string.Join(", ", commitmentExtensionIds));
			Console.WriteLine("preferenceIds = " + string.Join(", ", preferenceIds));
			Console.WriteLine("preferenceExtensionIds = " + string.Join(", ", preferenceIds));

			if (externalRelationNumber == -1)
			{
                throw new Exception("Something went wrong, not sure why. This error is triggered in the final check to make sure everything went okay. However, externalRelationNumber is still -1 (default).");
			}

            return externalRelationNumber;
        }

        private Int64 CreateExternalRelation(DateTime now)
        {
           string sql = "INSERT INTO ExternalRelation (NODB, NativeLanguageCode, NativeLanguageNODB, VIPNODB, IsTransfer, NationalOfficeNODB, SegmentNODB, IsCorrespondenceReceive, IsArchived, CreatedByUserName, CreatedByNODB, CreatedOn, LastUpdatedByUserName, LastUpdatedByNODB, LastUpdatedOn) VALUES (@NODB, @NativeLanguageCode, @NativeLanguageNODB, @VIPNODB, @IsTransfer, @NationalOfficeNODB, @SegmentNODB, @IsCorrespondenceReceive, @IsArchived, @CreatedByUserName, @CreatedByNODB, @CreatedOn, @LastUpdatedByUserName, @LastUpdatedByNODB, @LastUpdatedOn); SELECT SCOPE_IDENTITY()";

            Int64 externalRelationNumber;

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(sql, this.connection))
            {
                command.Transaction = this.transaction;

                command.Parameters.AddWithValue("@NODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@NativeLanguageCode", this.configuration["PlanSys:DefaultPrimaryLanguage"]);
                command.Parameters.AddWithValue("@NativeLanguageNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@VIPNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@IsTransfer", "N");
                command.Parameters.AddWithValue("@NationalOfficeNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@SegmentNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@IsCorrespondenceReceive", "Y");
                command.Parameters.AddWithValue("@IsArchived", "N");
                command.Parameters.AddWithValue("@CreatedByUsername", this.configuration["PlanSys:Username"]);
                command.Parameters.AddWithValue("@CreatedByNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@CreatedOn", now);
                command.Parameters.AddWithValue("@LastUpdatedByUsername", this.configuration["PlanSys:Username"]);
                command.Parameters.AddWithValue("@LastUpdatedByNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@LastUpdatedOn", now);

                try
                {
                    string identity = command.ExecuteScalar().ToString();

                    if (!Int64.TryParse(identity, out externalRelationNumber))
                    {
                        throw new Exception("Could not retrieve ExternalRelationNumber after inserting row into ExternalRelation");
                    }
                }
                catch (Exception exception)
                {
                    throw exception;
                }
            }

            return externalRelationNumber;
        }

		private long CreateExternalRelationAddress(Sponsor sponsor, Int64 externalRelationNumber, DateTime now)
        {

            string sql = "INSERT INTO ExternalRelationAddress (NODB, ExternalRelationNumber, ExternalRelationNumberNODB, Name1PrefixNODB, Name2PrefixNODB, Name1TitleCode, Name1TitleNODB, Name2TitleNODB, Name1Line1, Name1Line2, Salutation, CountryCode, CountryNODB, AppartmentNumber, HouseNumber, Street, Town, CountyNODB, PostCode, PostNODB, CByO, AddressTypeCode, AddressTypeNODB, Mobile, PrimaryEmail, CategoryCode, CategoryNODB, IsUnknown, IsCurrentAddress, PreviousAddressId, PreviousAddressNODB, IsActive, CreatedByUsername, CreatedByNODB, CreatedOn, LastUpdatedByUsername, LastUpdatedByNODB, LastUpdatedOn) VALUES (@NODB, @ExternalRelationNumber, @ExternalRelationNumberNODB, @Name1PrefixNODB, @Name2PrefixNODB, @Name1TitleCode, @Name1TitleNODB, @Name2TitleNODB, @Name1Line1, @Name1Line2, @Salutation, @CountryCode, @CountryNODB, @AppartmentNumber, @HouseNumber, @Street, @Town, @CountyNODB, @PostCode, @PostNODB, @CByO,  @AddressTypeCode, @AddressTypeNODB, @Mobile, @PrimaryEmail, @CategoryCode, @CategoryNODB, @IsUnknown, @IsCurrentAddress, @PreviousAddressId, @PreviousAddressNODB, @IsActive, @CreatedByUsername, @CreatedByNODB, @CreatedOn, @LastUpdatedByUsername, @LastUpdatedByNODB, @LastUpdatedOn); SELECT SCOPE_IDENTITY()";

            long addressId;

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(sql, this.connection))
            {
                command.Transaction = this.transaction;

                command.Parameters.AddWithValue("@NODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@ExternalRelationNumber", externalRelationNumber);
                command.Parameters.AddWithValue("@ExternalRelationNumberNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@Name1PrefixNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@Name2PrefixNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@Name1TitleCode", DBNull.Value);
                command.Parameters.AddWithValue("@Name1TitleNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@Name2TitleNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@Name1Line1", sponsor.forename);
                command.Parameters.AddWithValue("@Name1Line2", sponsor.surname);
                command.Parameters.AddWithValue("@Salutation", sponsor.forename + " " + sponsor.surname);
                command.Parameters.AddWithValue("@CountryCode", String.IsNullOrEmpty(sponsor.countryCode) ? "NOR" : sponsor.countryCode);
                command.Parameters.AddWithValue("@CountryNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@AppartmentNumber", String.IsNullOrEmpty(sponsor.appartmentNumber) ? (object)DBNull.Value : sponsor.appartmentNumber);
                command.Parameters.AddWithValue("@HouseNumber", String.IsNullOrEmpty(sponsor.houseNumber) ? (object)DBNull.Value : sponsor.houseNumber);
                command.Parameters.AddWithValue("@Street", sponsor.street);
                command.Parameters.AddWithValue("@Town", sponsor.town);
                command.Parameters.AddWithValue("@CountyNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@PostCode", sponsor.postCode);
                command.Parameters.AddWithValue("@CByO", String.IsNullOrEmpty(sponsor.cByO) ? (object)DBNull.Value : sponsor.cByO);
                command.Parameters.AddWithValue("@PostNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@AddressTypeCode", "N");
                command.Parameters.AddWithValue("@AddressTypeNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@Mobile", sponsor.phoneNumber);
                command.Parameters.AddWithValue("@PrimaryEmail", sponsor.eMailAddress);
                command.Parameters.AddWithValue("@CategoryCode", "IN");
                command.Parameters.AddWithValue("@CategoryNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@IsUnknown", "N");
                command.Parameters.AddWithValue("@IsCurrentAddress", "Y");
                command.Parameters.AddWithValue("@PreviousAddressId", DBNull.Value);
                command.Parameters.AddWithValue("@PreviousAddressNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@IsActive", "Y");
                command.Parameters.AddWithValue("@CreatedByUsername", this.configuration["PlanSys:Username"]);
                command.Parameters.AddWithValue("@CreatedByNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@CreatedOn", now);
                command.Parameters.AddWithValue("@LastUpdatedByUsername", this.configuration["PlanSys:Username"]);
                command.Parameters.AddWithValue("@LastUpdatedByNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@LastUpdatedOn", now);

                try
                {
                    string identity = command.ExecuteScalar().ToString();

                    if (!Int64.TryParse(identity, out addressId))
                    {
                        throw new Exception("Could not retrieve AddressId after inserting row into ExternalRelationAddress");
                    }
                }
                catch (Exception exception)
                {
                    throw exception;
                }
            }

            return addressId;
        }

        private void CreateExternalRelationLanguage(Int64 externalRealtionNumber, DateTime now)
        {
            string sql = "INSERT INTO ExternalRelationLanguage (ExternalRelationNODB, ExternalRelationNumber, LanguageID, LanguageNODB, CreatedByUserName, CreatedByNODB, CreatedOn, LastUpdatedByUserName, LastUpdatedByNODB, LastUpdatedOn) VALUES (@ExternalRelationNODB, @ExternalRelationNumber, @LanguageID, @LanguageNODB, @CreatedByUserName, @CreatedByNODB, @CreatedOn, @LastUpdatedByUserName, @LastUpdatedByNODB, @LastUpdatedOn);";

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(sql, this.connection))
            {
                command.Transaction = this.transaction;

                command.Parameters.AddWithValue("@ExternalRelationNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@ExternalRelationNumber", externalRealtionNumber);
				command.Parameters.AddWithValue("@LanguageID", this.configuration["PlanSys:DefaultSecondaryLanguage"]);
                command.Parameters.AddWithValue("@LanguageNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@CreatedByUsername", this.configuration["PlanSys:Username"]);
                command.Parameters.AddWithValue("@CreatedByNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@CreatedOn", now);
                command.Parameters.AddWithValue("@LastUpdatedByUsername", this.configuration["PlanSys:Username"]);
                command.Parameters.AddWithValue("@LastUpdatedByNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@LastUpdatedOn", now);

                try
                {
                    command.ExecuteNonQuery();
                }
                catch (Exception Exception)
                {
                    throw Exception;
                }
            }
        }

        private long CreatePersonalId(Sponsor sponsor, DateTime now)
        {

            string sql = "INSERT INTO PersonalId (PersonalId, DOB, NODB, CreatedByUserName, CreatedByNODB, CreatedOn, LastUpdatedByUserName, LastUpdatedByNODB, LastUpdatedOn, IsBlocked) VALUES (@PersonalId, @DOB, @NODB, @CreatedByUserName, @CreatedByNODB, @CreatedOn, @LastUpdatedByUserName, @LastUpdatedByNODB, @LastUpdatedOn, @IsBlocked); SELECT SCOPE_IDENTITY()";

            long Pid;

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(sql, this.connection))
            {
                command.Transaction = this.transaction;

                command.Parameters.AddWithValue("@PersonalId", String.IsNullOrEmpty(sponsor.ssn) ? (object)DBNull.Value : sponsor.ssn);
                command.Parameters.AddWithValue("@DOB", sponsor.dateOfBirth.HasValue ? sponsor.dateOfBirth : (object)DBNull.Value);
                command.Parameters.AddWithValue("@NODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@CreatedByUserName", this.configuration["PlanSys:Username"]);
                command.Parameters.AddWithValue("@CreatedByNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@CreatedOn", now);
                command.Parameters.AddWithValue("@LastUpdatedByUserName", this.configuration["PlanSys:Username"]);
                command.Parameters.AddWithValue("@LastUpdatedByNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@LastUpdatedOn", now);
                command.Parameters.AddWithValue("@IsBlocked", "N");

                try
                {
                    string Identity = command.ExecuteScalar().ToString();

                    if (!Int64.TryParse(Identity, out Pid))
                    {
                        throw new Exception("Could not retrieve Pid after inserting row into PersonalId");
                    }
                }
                catch (Exception Exception)
                {
                    throw Exception;
                }
            }

            return Pid;
        }

        private long CreateExternalRelationAddressPersonalId(Int64 addressId, Int64 pid, DateTime now)
        {

            string sql = "INSERT INTO ExternalRelationAddressPersonalId (Pid, AddressId, NODB, CreatedByUserName, CreatedByNODB, CreatedOn, LastUpdatedByUserName, LastUpdatedByNODB, LastUpdatedOn) VALUES (@Pid, @AddressId, @NODB, @CreatedByUserName, @CreatedByNODB, @CreatedOn, @LastUpdatedByUserName, @LastUpdatedByNODB, @LastUpdatedOn); SELECT SCOPE_IDENTITY()";

            long ExternalRelationAddressPersonalId;

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(sql, this.connection))
            {
                command.Transaction = this.transaction;

                command.Parameters.AddWithValue("@Pid", pid);
                command.Parameters.AddWithValue("@AddressId", addressId);
                command.Parameters.AddWithValue("@NODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@CreatedByUserName", this.configuration["PlanSys:Username"]);
                command.Parameters.AddWithValue("@CreatedByNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@CreatedOn", now);
                command.Parameters.AddWithValue("@LastUpdatedByUserName", this.configuration["PlanSys:Username"]);
                command.Parameters.AddWithValue("@LastUpdatedByNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@LastUpdatedOn", now);

                try
                {
                    string Identity = command.ExecuteScalar().ToString();

                    if (!Int64.TryParse(Identity, out ExternalRelationAddressPersonalId))
                    {
                        throw new Exception("Could not retrieve ExternalRelationAddressPersonalId after inserting row into ExternalRelationAddressPersonalId");
                    }
                }
                catch (Exception Exception)
                {
                    throw Exception;
                }
            }

            return ExternalRelationAddressPersonalId;
        }

        private long CreateExternalRelationPaymentMethod(Sponsor sponsor, Int64 externalRelationNumber, DateTime now)
        {
            string sql = "INSERT INTO ExternalRelationPaymentMethod (NODB, ExternalRelationNumber, ExternalRelationNumberNODB, PaymentFrequencyCode, PaymentFrequencyNODB, IsUseAnotherAccount, MonthlyPaymentDayNumber, AccountNODB, PaymentTypeCode, PaymentTypeCodeNODB, PaymentSequenceNumber, CreatedByUserName, CreatedByNODB, CreatedOn, LastUpdatedByUserName, LastUpdatedByNODB, LastUpdatedOn) VALUES (@NODB, @ExternalRelationNumber, @ExternalRelationNumberNODB, @PaymentFrequencyCode, @PaymentFrequencyNODB, @IsUseAnotherAccount, @MonthlyPaymentDayNumber, @AccountNODB, @PaymentTypeCode, @PaymentTypeCodeNODB, @PaymentSequenceNumber, @CreatedByUserName, @CreatedByNODB, @CreatedOn, @LastUpdatedByUserName, @LastUpdatedByNODB, @LastUpdatedOn); SELECT SCOPE_IDENTITY()";

            long PaymentMethodId;

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(sql, this.connection))
            {
                command.Transaction = this.transaction;

                command.Parameters.AddWithValue("@NODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@ExternalRelationNumber", externalRelationNumber);
                command.Parameters.AddWithValue("@ExternalRelationNumberNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@PaymentFrequencyCode", 0);
                command.Parameters.AddWithValue("@PaymentFrequencyNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@IsUseAnotherAccount", "N");
                command.Parameters.AddWithValue("@MonthlyPaymentDayNumber", 1);
                command.Parameters.AddWithValue("@AccountNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@PaymentTypeCode", this.configuration["PlanSys:DefaultPaymentTypeCode"]);
                command.Parameters.AddWithValue("@PaymentTypeCodeNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@PaymentSequenceNumber", 1);
                command.Parameters.AddWithValue("@CreatedByUserName", this.configuration["PlanSys:Username"]);
                command.Parameters.AddWithValue("@CreatedByNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@CreatedOn", now);
                command.Parameters.AddWithValue("@LastUpdatedByUserName",this.configuration["PlanSys:Username"]);
                command.Parameters.AddWithValue("@LastUpdatedByNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@LastUpdatedOn", now);

                try
                {
                    string Identity = command.ExecuteScalar().ToString();

                    if (!Int64.TryParse(Identity, out PaymentMethodId))
                    {
                        throw new Exception("Could not retrieve PaymentMethodId after inserting row into ExternalRelationPaymentMethod");
                    }
                }
                catch (Exception Exception)
                {
                    throw Exception;
                }
            }

            return PaymentMethodId;
        }

		private Int64 CreateCommitment_ExtensionPS3(Sponsor sponsor, Int64 externalRelationNumber, Int64 commitmentId, DateTime now)
		{
			string sql = "INSERT INTO Commitment_ExtensionPS3 (CommitmentID, AssignmentMode, AssignmentStatus, NODB, CreatedByUserName, CreatedByNODB, CreatedOn, LastUpdatedByUserName, LastUpdatedByNODB, LastUpdatedOn) VALUES (@CommitmentID, @AssignmentMode, @AssignmentStatus, @NODB, @CreatedByUserName, @CreatedByNODB, @CreatedOn, @LastUpdatedByUserName, @LastUpdatedByNODB, @LastUpdatedOn); SELECT SCOPE_IDENTITY()";

            Int64 commitmentExtensionId;

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(sql, this.connection))
            {
                command.Transaction = this.transaction;

                command.Parameters.AddWithValue("@CommitmentID", commitmentId);
                command.Parameters.AddWithValue("@AssignmentMode", "AU");
                command.Parameters.AddWithValue("@AssignmentStatus", "PA");
                command.Parameters.AddWithValue("@NODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@CreatedByUserName", this.configuration["PlanSys:Username"]);
                command.Parameters.AddWithValue("@CreatedByNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@CreatedOn", now);
                command.Parameters.AddWithValue("@LastUpdatedByUserName", this.configuration["PlanSys:Username"]);
                command.Parameters.AddWithValue("@LastUpdatedByNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@LastUpdatedOn", now);
				command.Parameters.AddWithValue("@InvoiceMonth", (object)DBNull.Value);

                try
                {
                    string identity = command.ExecuteScalar().ToString();

                    if (!Int64.TryParse(identity, out commitmentExtensionId))
                    {
                        throw new Exception("Could not retrieve commitmentExtensionId after inserting row into Commitment_ExtensionPS3");
                    }
                }
                catch (Exception exception)
                {
                    throw exception;
                }
            }

            return commitmentExtensionId;
		}

		private void CreateCommitmentAmount(Sponsor sponsor, Int64 commitmentId, Preference preference, DateTime now)
		{
			string sql = "INSERT INTO CommitmentAmount (CommitmentID, CommitmentNODB, MonthlyAmount, CommitmentAmountStartDate, CreatedByUserName, CreatedByNODB, CreatedOn, LastUpdatedByUserName, LastUpdatedByNODB, LastUpdatedOn, IsAutoCommitAmount, DistributionValue, AmountEntered) VALUES (@CommitmentID, @CommitmentNODB, @MonthlyAmount, @CommitmentAmountStartDate, @CreatedByUserName, @CreatedByNODB, @CreatedOn, @LastUpdatedByUserName, @LastUpdatedByNODB, @LastUpdatedOn, @IsAutoCommitAmount, @DistributionValue, @AmountEntered)";
            
            int? amount = Int32.Parse(this.configuration["DefaultAmount"]);

            if (preference.amount.HasValue && preference.amount > Int32.Parse(this.configuration["MinimumAmount"]))
            {
                amount = preference.amount;
            }

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(sql, this.connection))
            {
                command.Transaction = this.transaction;

				// Start activity today. However, if today is after the 10th day of this month, then start next month.
                DateTime startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

                if (Convert.ToInt64(DateTime.Now.Day.ToString()) > 10)
                {
                    startDate.AddMonths(1);
                }

                command.Parameters.AddWithValue("@CommitmentID", commitmentId);
                command.Parameters.AddWithValue("@CommitmentNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@MonthlyAmount", amount);
                command.Parameters.AddWithValue("@CommitmentAmountStartDate", startDate);
                command.Parameters.AddWithValue("@CreatedByUserName", this.configuration["PlanSys:Username"]);
                command.Parameters.AddWithValue("@CreatedByNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@CreatedOn", now);
                command.Parameters.AddWithValue("@LastUpdatedByUserName", this.configuration["PlanSys:Username"]);
                command.Parameters.AddWithValue("@LastUpdatedByNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@LastUpdatedOn", now);
                command.Parameters.AddWithValue("@IsAutoCommitAmount", 0);
                command.Parameters.AddWithValue("@DistributionValue", 0);
				command.Parameters.AddWithValue("@AmountEntered", amount);


                try
                {
                    command.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    throw exception;
                }
            }
		}

		private Int64 CreateCommitment(Sponsor sponsor, Int64 externalRelationNumber, Int64 paymentMethodId, DateTime now)
		{
			string sql = "INSERT INTO Commitment (NODB, PaymentMethodID, PaymentMethodNODB, CommitmentType, CommitmentTypeNODB, ExternalRelationNumber, ExternalRelationNODB, StartDate, EndDate, IsActive, AccountTypeCode, AccountTypeNODB, CreatedByUserName, CreatedByNODB, CreatedOn, LastUpdatedByUserName, LastUpdatedByNODB, LastUpdatedOn, InvoiceMonth) VALUES (@NODB, @PaymentMethodID, @PaymentMethodNODB, @CommitmentType, @CommitmentTypeNODB, @ExternalRelationNumber, @ExternalRelationNODB, @StartDate, @EndDate, @IsActive, @AccountTypeCode, @AccountTypeNODB, @CreatedByUserName, @CreatedByNODB, @CreatedOn, @LastUpdatedByUserName, @LastUpdatedByNODB, @LastUpdatedOn, @InvoiceMonth); SELECT SCOPE_IDENTITY()";

            Int64 commitmentId;

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(sql, this.connection))
            {
                command.Transaction = this.transaction;


				// Start commitment today. However, if today is after the 10th day of this month, then start next month.
                DateTime startDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

                if (Convert.ToInt64(DateTime.Now.Day.ToString()) > 10)
                {
                    startDate.AddMonths(1);
                }

                command.Parameters.AddWithValue("@NODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@PaymentMethodID", paymentMethodId);
                command.Parameters.AddWithValue("@PaymentMethodNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@CommitmentType", "RE");
                command.Parameters.AddWithValue("@CommitmentTypeNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@ExternalRelationNumber", externalRelationNumber);
                command.Parameters.AddWithValue("@ExternalRelationNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@StartDate", startDate);
                command.Parameters.AddWithValue("@EndDate", (object)DBNull.Value);
                command.Parameters.AddWithValue("@IsActive", "Y");
                command.Parameters.AddWithValue("@AccountTypeCode", this.configuration["PlanSys:DefaultAccountTypeCode"]);
				command.Parameters.AddWithValue("@AccountTypeNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@CreatedByUserName", this.configuration["PlanSys:Username"]);
                command.Parameters.AddWithValue("@CreatedByNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@CreatedOn", now);
                command.Parameters.AddWithValue("@LastUpdatedByUserName", this.configuration["PlanSys:Username"]);
                command.Parameters.AddWithValue("@LastUpdatedByNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@LastUpdatedOn", now);
				command.Parameters.AddWithValue("@InvoiceMonth", (object)DBNull.Value);

                try
                {
                    string identity = command.ExecuteScalar().ToString();

                    if (!Int64.TryParse(identity, out commitmentId))
                    {
                        throw new Exception("Could not retrieve commitmentId after inserting row into Commitment");
                    }
                }
                catch (Exception exception)
                {
                    throw exception;
                }
            }

            return commitmentId;
		}

        private Int64 CreateActivity(Sponsor sponsor, Int64 externalRelationNumber, Int64 PaymentMethodId, DateTime now)
        {
            string sql = "INSERT INTO Activity (NODB, SourceCode, SourceNODB, ActivityTypeCode, ActivityTypeNODB, ActivityStartDate, ContactNODB, ExternalRelationNumber, ExternalRelationNumberNODB, OriginalSCCount, IsActive, HandlingDate, PaymentMethodID, PaymentMethodNODB, IsConverted, CreatedByUserName, CreatedByNODB, CreatedOn, LastUpdatedByUserName, LastUpdatedByNODB, LastUpdatedOn) VALUES (@NODB, @SourceCode, @SourceNODB, @ActivityTypeCode, @ActivityTypeNODB, @ActivityStartDate, @ContactNODB, @ExternalRelationNumber, @ExternalRelationNumberNODB, @OriginalSCCount, @IsActive, @HandlingDate, @PaymentMethodID, @PaymentMethodNODB, @IsConverted, @CreatedByUserName, @CreatedByNODB, @CreatedOn, @LastUpdatedByUserName, @LastUpdatedByNODB, @LastUpdatedOn); SELECT SCOPE_IDENTITY()";

            Int64 activityId;

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(sql, this.connection))
            {
                command.Transaction = this.transaction;


				// Start activity today. However, if today is after the 10th day of this month, then start next month.
                DateTime activityStartDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);

                if (Convert.ToInt64(DateTime.Now.Day.ToString()) > 10)
                {
                    activityStartDate.AddMonths(1);
                }

                command.Parameters.AddWithValue("@NODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@SourceCode", sponsor.sourceCode);
                command.Parameters.AddWithValue("@SourceNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@ActivityTypeCode", "SP");
                command.Parameters.AddWithValue("@ActivityTypeNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@ActivityStartDate", activityStartDate);
                command.Parameters.AddWithValue("@ContactNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@ExternalRelationNumber", externalRelationNumber);
                command.Parameters.AddWithValue("@ExternalRelationNumberNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@OriginalSCCount", sponsor.preferences.Count());
                command.Parameters.AddWithValue("@IsActive", "Y");
                command.Parameters.AddWithValue("@HandlingDate", now);
                command.Parameters.AddWithValue("@PaymentMethodID", PaymentMethodId);
                command.Parameters.AddWithValue("@PaymentMethodNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@IsConverted", "N");
                command.Parameters.AddWithValue("@CreatedByUserName", this.configuration["PlanSys:Username"]);
                command.Parameters.AddWithValue("@CreatedByNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@CreatedOn", now);
                command.Parameters.AddWithValue("@LastUpdatedByUserName", this.configuration["PlanSys:Username"]);
                command.Parameters.AddWithValue("@LastUpdatedByNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@LastUpdatedOn", now);

                try
                {
                    string identity = command.ExecuteScalar().ToString();

                    if (!Int64.TryParse(identity, out activityId))
                    {
                        throw new Exception("Could not retrieve ActivityId after inserting row into Activity");
                    }
                }
                catch (Exception exception)
                {
                    throw exception;
                }
            }

            return activityId;
        }

        private Int64 CreateActivity_ExtensionPS3(Sponsor sponsor, Int64 activityId, DateTime now)
        {
            Int64 activityExtensionId;

            string sql = "INSERT INTO Activity_ExtensionPS3 (NODB, ActivityId, ActivityIDNODB, SourceCode, SourceCodeNODB, CreatedByUserName, CreatedByNODB, CreatedOn, LastUpdatedByUserName, LastUpdatedByNODB, LastUpdatedOn) VALUES (@NODB, @ActivityId, @ActivityIDNODB, @SourceCode, @SourceCodeNODB, @CreatedByUserName, @CreatedByNODB, @CreatedOn, @LastUpdatedByUserName, @LastUpdatedByNODB, @LastUpdatedOn); SELECT SCOPE_IDENTITY()";

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(sql, this.connection))
            {
                command.Transaction = this.transaction;

                command.Parameters.AddWithValue("@NODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@ActivityId", activityId);
                command.Parameters.AddWithValue("@ActivityIDNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@SourceCode", sponsor.sourceCode);
                command.Parameters.AddWithValue("@SourceCodeNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@CreatedByUserName", this.configuration["PlanSys:Username"]);
                command.Parameters.AddWithValue("@CreatedByNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@CreatedOn", now);
                command.Parameters.AddWithValue("@LastUpdatedByUserName", this.configuration["PlanSys:Username"]);
                command.Parameters.AddWithValue("@LastUpdatedByNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@LastUpdatedOn", now);

                try
                {
                    string identity = command.ExecuteScalar().ToString();

                    if (!Int64.TryParse(identity, out activityExtensionId))
                    {
                        throw new Exception("Could not retrieve activityExtensionId after inserting row into Activity_ExtensionPS3");
                    }
                }
                catch (Exception exception)
                {
                    throw exception;
                }
            }

            return activityExtensionId;
        }

		private long CreatePreference_ExtensionPS3(Int64 preferenceId, Int64 externalRealtionNumber, DateTime now)
		{
			string sql = "INSERT INTO Preference_ExtensionPS3 (PreferenceId, ExternalRelationNumber, NODB, CreatedByUserName, CreatedByNODB, CreatedOn, LastUpdatedByUserName, LastUpdatedByNODB, LastUpdatedOn) VALUES (@PreferenceId, @ExternalRelationNumber, @NODB, @CreatedByUserName, @CreatedByNODB, @CreatedOn, @LastUpdatedByUserName, @LastUpdatedByNODB, @LastUpdatedOn); SELECT SCOPE_IDENTITY()";

            Int64 preferenceExtensionId;

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(sql, this.connection))
            {
                command.Transaction = this.transaction;

                command.Parameters.AddWithValue("@PreferenceId", preferenceId);
				command.Parameters.AddWithValue("@ExternalRelationNumber", externalRealtionNumber);
                command.Parameters.AddWithValue("@NODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@CreatedByUserName", this.configuration["PlanSys:Username"]);
                command.Parameters.AddWithValue("@CreatedByNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@CreatedOn", now);
                command.Parameters.AddWithValue("@LastUpdatedByUserName", this.configuration["PlanSys:Username"]);
                command.Parameters.AddWithValue("@LastUpdatedByNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@LastUpdatedOn", now);

                try
                {
                    string identity = command.ExecuteScalar().ToString();

                    if (!Int64.TryParse(identity, out preferenceExtensionId))
                    {
                        throw new Exception("Could not retrieve preferenceExtensionId after inserting row into Preference_ExtensionPS3");
                    }
                }
                catch (Exception exception)
                {
                    throw exception;
                }
            }

            return preferenceExtensionId;
		}

        private long CreatePreference(Preference preference, Sponsor sponsor, Int64 activityId, Int64 paymentMethodId, Int64 commitmentId, DateTime now)
        {
			// removed @Amount
            string sql = "INSERT INTO Preference (NODB, ActivityID, ActivityNODB, PaymentMethodID, PaymentMethodNODB, DocumentNODB, TopicCode, TopicNODB, SourceCode, SourceNODB, LanguageNODB, ReligionNODB, FieldOfficeNODB, ContinentCode, ContinentNODB, Description, Sex, AgeFrom, AgeTo, AssignmentMode, PreferenceStatus, PreferenceStatusHandlingDate, Amount, CommitmentNODB, AssignmentDocumentNODB, IsPreConnection, PreConnectionNODB, CreatedByUserName, CreatedByNODB, CreatedOn, LastUpdatedByUserName, LastUpdatedByNODB, LastUpdatedOn) VALUES (@NODB, @ActivityID, @ActivityNODB, @PaymentMethodID, @PaymentMethodNODB, @DocumentNODB, @TopicCode, @TopicNODB, @SourceCode, @SourceNODB, @LanguageNODB, @ReligionNODB, @FieldOfficeNODB, @ContinentCode, @ContinentNODB, @Description, @Sex, @AgeFrom, @AgeTo, @AssignmentMode, @PreferenceStatus, @PreferenceStatusHandlingDate, @Amount, @CommitmentNODB, @AssignmentDocumentNODB, @IsPreConnection, @PreConnectionNODB, @CreatedByUserName, @CreatedByNODB, @CreatedOn, @LastUpdatedByUserName, @LastUpdatedByNODB, @LastUpdatedOn); SELECT SCOPE_IDENTITY()";
			//string sql = "INSERT INTO Preference (NODB, ActivityID, ActivityNODB, PaymentMethodID, PaymentMethodNODB, DocumentNODB, TopicCode, TopicNODB, SourceCode, SourceNODB, LanguageNODB, ReligionNODB, FieldOfficeNODB, ContinentCode, ContinentNODB, Description, Sex, AgeFrom, AgeTo, AssignmentMode, PreferenceStatus, PreferenceStatusHandlingDate, CommitmentNODB, AssignmentDocumentNODB, IsPreConnection, PreConnectionNODB, CreatedByUserName, CreatedByNODB, CreatedOn, LastUpdatedByUserName, LastUpdatedByNODB, LastUpdatedOn) VALUES (@NODB, @ActivityID, @ActivityNODB, @PaymentMethodID, @PaymentMethodNODB, @DocumentNODB, @TopicCode, @TopicNODB, @SourceCode, @SourceNODB, @LanguageNODB, @ReligionNODB, @FieldOfficeNODB, @ContinentCode, @ContinentNODB, @Description, @Sex, @AgeFrom, @AgeTo, @AssignmentMode, @PreferenceStatus, @PreferenceStatusHandlingDate, @CommitmentNODB, @AssignmentDocumentNODB, @IsPreConnection, @PreConnectionNODB, @CreatedByUserName, @CreatedByNODB, @CreatedOn, @LastUpdatedByUserName, @LastUpdatedByNODB, @LastUpdatedOn); SELECT SCOPE_IDENTITY()";

            Int64 preferenceId;

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(sql, this.connection))
            {
                command.Transaction = this.transaction;

                command.Parameters.AddWithValue("@NODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@ActivityID", activityId);
                command.Parameters.AddWithValue("@ActivityNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@PaymentMethodID", paymentMethodId);
                command.Parameters.AddWithValue("@PaymentMethodNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@DocumentNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@TopicCode", "ENR");
                command.Parameters.AddWithValue("@TopicNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@SourceCode", sponsor.sourceCode);
                command.Parameters.AddWithValue("@SourceNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@LanguageNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@ReligionNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@FieldOfficeNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@ContinentCode", String.IsNullOrEmpty(preference.continentCode) ? (object)DBNull.Value : preference.continentCode);
                command.Parameters.AddWithValue("@ContinentNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@Description", "WebService  ; ^^NOPreference:  Age " + preference.ageFrom + " - " + preference.ageTo + ", Gender " + preference.gender + " , ^^");
                command.Parameters.AddWithValue("@Sex", String.IsNullOrEmpty(preference.gender) ? (object)DBNull.Value : preference.gender);
                command.Parameters.AddWithValue("@AgeFrom", !preference.ageFrom.HasValue ? (object)DBNull.Value : preference.ageFrom);
                command.Parameters.AddWithValue("@AgeTo", !preference.ageTo.HasValue ? (object)DBNull.Value : preference.ageTo);
                command.Parameters.AddWithValue("@AssignmentMode", "AU");
                command.Parameters.AddWithValue("@PreferenceStatus", "OP");
                command.Parameters.AddWithValue("@PreferenceStatusHandlingDate", now);
                command.Parameters.AddWithValue("@Amount", preference.amount);
                command.Parameters.AddWithValue("@CommitmentId", commitmentId);
                command.Parameters.AddWithValue("@CommitmentNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@AssignmentDocumentNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@IsPreConnection", "N");
                command.Parameters.AddWithValue("@PreConnectionNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@CreatedByUserName", this.configuration["PlanSys:Username"]);
                command.Parameters.AddWithValue("@CreatedByNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@CreatedOn", now);
                command.Parameters.AddWithValue("@LastUpdatedByUserName", this.configuration["PlanSys:Username"]);
                command.Parameters.AddWithValue("@LastUpdatedByNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@LastUpdatedOn", now);

                try
                {
                    string identity = command.ExecuteScalar().ToString();

                    if (!Int64.TryParse(identity, out preferenceId))
                    {
                        throw new Exception("Could not retrieve PreferenceId after inserting row into Preference");
                    }
                }
                catch (Exception exception)
                {
                    throw exception;
                }
            }

            return preferenceId;
        }

        private void CreatePreferenceCountry(Preference preference, Int64 preferenceId, DateTime now)
        {
            string sql = "INSERT INTO PreferenceCountry (CountryNODB, CountryCode, PreferenceID, PreferenceNODB, PreferenceSequence, CreatedByUserName, CreatedByNODB, CreatedOn, LastUpdatedByUserName, LastUpdatedByNODB, LastUpdatedOn) VALUES (@CountryNODB, @CountryCode, @PreferenceID, @PreferenceNODB, @PreferenceSequence, @CreatedByUserName, @CreatedByNODB, @CreatedOn, @LastUpdatedByUserName, @LastUpdatedByNODB, @LastUpdatedOn); SELECT SCOPE_IDENTITY()";

            using (System.Data.SqlClient.SqlCommand command = new System.Data.SqlClient.SqlCommand(sql, this.connection))
            {
                command.Transaction = this.transaction;

                command.Parameters.AddWithValue("@CountryNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@CountryCode", preference.countryCode);
                command.Parameters.AddWithValue("@PreferenceID", preferenceId);
                command.Parameters.AddWithValue("@PreferenceNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@PreferenceSequence", 1);
                command.Parameters.AddWithValue("@CreatedByUserName", this.configuration["PlanSys:Username"]);
                command.Parameters.AddWithValue("@CreatedByNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@CreatedOn", now);
                command.Parameters.AddWithValue("@LastUpdatedByUserName", this.configuration["PlanSys:Username"]);
                command.Parameters.AddWithValue("@LastUpdatedByNODB", this.configuration["PlanSys:NODB"]);
                command.Parameters.AddWithValue("@LastUpdatedOn", now);

                try
                {
                    command.ExecuteNonQuery();
                }
                catch (Exception Exception)
                {
                    throw Exception;
                }
            }
        }
    }
}