using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Novell.Directory.Ldap;

namespace Ps3Api.Misc{
    public class LDAP{

        private string server;
        private int port;
        private int timelimit;
        private string domain;
        private string suffix;

        private string searchBase;
        private string searchFilter;

        private LdapConnection connection;

        private System.Diagnostics.Stopwatch sw;

        public LDAP(string server, int port, int timelimit, string domain, string suffix) {
            this.server = server;
            this.port = port;
            this.timelimit = timelimit;
            this.domain = domain;
            this.suffix = suffix;

            this.connection = new LdapConnection();
			this.connection.SecureSocketLayer = true; // This likely means port 636 instead of 389

            this.sw = new System.Diagnostics.Stopwatch();
        }

        public Boolean Connect()
        {

            try{
                this.sw.Reset();
                this.sw.Start();
                this.connection.Connect(this.server, this.port);
                this.sw.Stop();
                Console.WriteLine("LDAP.Connect() - Ticks: " + this.sw.ElapsedTicks + " ms: " + this.sw.ElapsedMilliseconds);
            }
            catch (LdapException exception)
            {
                Console.WriteLine("Connect(): " + exception.Message);
                throw exception;
            }

            Console.WriteLine("LDAP: Connected");
            return true;
        }

        public Boolean Bind(string username, string password)
        {
            try {
                this.sw.Reset();
                this.sw.Start();
                this.connection.Bind(this.domain + "\\" + username, password);
                this.sw.Stop();
                Console.WriteLine("LDAP.Bind() - Ticks: " + this.sw.ElapsedTicks + " ms: " + this.sw.ElapsedMilliseconds);
            }
            catch(LdapException exception)
            {
                Console.WriteLine("Bind(): " + exception.Message);
                throw exception;
            }
            Console.WriteLine("LDAP: Bound");
            return true;
        }

        public LdapEntry SearchAccountName(string accountName)
        {
            this.searchFilter = String.Format("(&(objectClass=User)(sAMAccountName={0}))",
				accountName
			);

            this.searchBase = string.Format("dc={0},dc={1}",
				this.domain,
				this.suffix
			);

            LdapSearchConstraints constraints = new LdapSearchConstraints();

            //constraints.TimeLimit = this.timelimit; // I get issues when specifying this. Seems like we get problems even if we are not even close to the timeout threshold.

            Novell.Directory.Ldap.LdapSearchResults results = null;


            try
            {
                this.sw.Reset();
                this.sw.Start();
                results = this.connection.Search(
                    this.searchBase,
                    LdapConnection.SCOPE_SUB,
                    this.searchFilter,
                    null,
                    false,
                    constraints
                );

                this.sw.Stop();
                Console.WriteLine("LDAP.Search() - Ticks: " + this.sw.ElapsedTicks + " ms: " + this.sw.ElapsedMilliseconds);
            }
            catch(Exception exception)
            {
                Console.WriteLine("Search(): " + exception.Message);
                throw exception;
            }


            LdapEntry nextEntry = null;

            while (results.HasMore())
            {
                try{
                    nextEntry = results.Next();
                    break;
                }
                catch(LdapException exception)
                {
                    Console.WriteLine(exception.Message);
                    continue;
                }
            }

            return nextEntry;
        }

        public Boolean IsEntryMemberOf(LdapEntry ldapEntry, string groupToSearchFor)
        {
            LdapAttribute memberOf = ldapEntry.getAttribute("memberOf");

            string [] values = memberOf.StringValueArray;

            Console.WriteLine("Checking memberOf");

            foreach (string value in values)
            {
                string group = value.Split(",")[0].Split("=")[1];

                Console.WriteLine(group + " vs. " + groupToSearchFor);

                if (group == groupToSearchFor)
                {
                    return true;
                }
            }

            return false;
        }

        public void Close()
        {
            this.connection.Disconnect();
            this.connection.Dispose();
        }
    }
}