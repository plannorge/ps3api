using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authorization;
using Ps3Api.Models;

namespace Ps3Api.Misc{
    public class Validation{
        private IConfiguration configuration;
        public Validation(IConfiguration configuration) {
            this.configuration = configuration;
        }

        public Boolean isValid(string type, object validationObject)
        {
            string file = string.Format("data/rules/{0}.json", type);
            string fileContent = System.IO.File.ReadAllText(file);

            Dictionary<string, Rule> rules = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, Rule>>(fileContent);

            foreach(KeyValuePair<string, Rule> entry in rules)
            {
                switch (entry.Value.type)
                {
                    case "string":
                        this.isValidString(entry.Key, entry.Value, validationObject);
                        break;
                    case "DateTime":
                        this.isValidDateTime(entry.Key, entry.Value, validationObject);
                        break;
                    case "email":
                        this.isValidEMail(entry.Key, entry.Value, validationObject);
                        break;
                    case "int":
                        this.isValidInt(entry.Key, entry.Value, validationObject);
                        break;
                    case "ssn":
                        this.isValidSSN(entry.Key, entry.Value, validationObject);
                        break;
                }
            }

            return true;
        }

        private Boolean isValidEMail(string variable, Rule rule, object validationObject)
        {
            string value = (string) validationObject.GetType().GetProperty(variable).GetValue(validationObject, null);

            if (rule.allowEmpty && String.IsNullOrEmpty(value))
            {
                return true;
            }

            if (rule.maxLength.HasValue && value.Length > rule.maxLength.Value)
            {
                throw new Exception(
                    string.Format(
                        "The field [{0}] has a maximum length of [{1}]. The provided [{0}] has a length of [{2}].",
                        variable,
                        rule.maxLength.Value.ToString(),
                        value.Length
                    )
                );
            }

            if (rule.minLength.HasValue && value.Length < rule.minLength.Value)
            {
                throw new Exception(
                    string.Format(
                        "The field [{0}] has a minimum length of [{1}]. The provided [{0}] has a length of [{2}].",
                        variable,
                        rule.minLength.Value.ToString(),
                        value.Length
                    )
                );
            }

            var addr = new System.Net.Mail.MailAddress(value);

            if (addr.Address != value)
            {
                throw new Exception("The system did not recognize the value provided in the field [eMailAddress] address as a valid e-mail address.");
            }

            return true;
        }

        private Boolean isValidDateTime(string variable, Rule rule, object validationObject)
        {
            DateTime? value = null;

            if (value != null)
            {
                value = (DateTime?) validationObject;
            }

            if (rule.allowEmpty && !value.HasValue)
            {
                return true;
            }

            DateTime now = DateTime.Today;
            int age = now.Year - value.Value.Year;

            if (rule.maxYearDifference.HasValue && age > rule.maxYearDifference.Value)
            {
                throw new Exception(
                    String.Format(
                        "The calculated year difference [{0}] between today and [{1}] exceeds the maximum threshold of [{2}].",
                        age.ToString(),
                        variable,
                        rule.maxYearDifference.Value.ToString()
                    )
                );
            }

            if (rule.minYearDifference.HasValue && age < rule.minYearDifference.Value)
            {
                throw new Exception(
                    String.Format(
                        "The calculated year difference [{0}] between today and [{1}] is below the minimum threshold of [{2}].",
                        age.ToString(),
                        variable,
                        rule.maxYearDifference.Value.ToString()
                    )
                );
            }

            return true;
        }

        private Boolean isValidInt(string variable, Rule rule, object validationObject)
        {
            int? value = null;
            int? compareValue = null;

            if (validationObject != null)
            {
                value = (int?) validationObject.GetType().GetProperty(variable).GetValue(validationObject, null);
            }

            if (rule.allowEmpty && !value.HasValue)
            {
                return true;
            }

            if (rule.maximum.HasValue && value.Value > rule.maximum.Value)
            {
                throw new Exception(
                    string.Format(
                        "The field [{0}] has an allowed maximum vale of [{1}]. The provided [{0}] has a value of [{2}].",
                        variable,
                        rule.maximum.Value.ToString(),
                        value.Value.ToString()
                    )
                );
            }

            if (rule.minimum.HasValue && value.Value < rule.minimum.Value)
            {
                throw new Exception(
                    string.Format(
                        "The field [{0}] has an allowed minimum value of [{1}]. The provided [{0}] has a value of [{2}].",
                        variable,
                        rule.minimum.Value.ToString(),
                        value.Value.ToString()
                    )
                );
            }

            if (!String.IsNullOrEmpty(rule.greaterThan))
            {
                compareValue = (int?) validationObject.GetType().GetProperty(rule.greaterThan).GetValue(validationObject, null);

                if (value <= compareValue)
                {
                    throw new Exception(
                        string.Format(
                            "The field [{0}][{1}] must be greater than the field [{2}][{3}].",
                            variable,
                            value,
                            rule.greaterThan,
                            compareValue
                        )
                    );
                }
            }

            if (!String.IsNullOrEmpty(rule.lessThan))
            {
                compareValue = (int?) validationObject.GetType().GetProperty(rule.lessThan).GetValue(validationObject, null);

                if (value >= compareValue)
                {
                    throw new Exception(
                        string.Format(
                            "The field [{0}][{1}] must be less than the field [{2}][{3}].",
                            variable,
                            value,
                            rule.lessThan,
                            compareValue
                        )
                    );
                }
            }

            if (!String.IsNullOrEmpty(rule.greaterThanOrEqualTo))
            {
                compareValue = (int?) validationObject.GetType().GetProperty(rule.greaterThanOrEqualTo).GetValue(validationObject, null);

                if (value < compareValue)
                {
                    throw new Exception(
                        string.Format(
                            "The field [{0}][{1}] must be greater than or equal to the field [{2}][{3}].",
                            variable,
                            value,
                            rule.greaterThanOrEqualTo,
                            compareValue
                        )
                    );
                }
            }

            if (!String.IsNullOrEmpty(rule.lessThanOrEqualTo))
            {
                compareValue = (int?) validationObject.GetType().GetProperty(rule.lessThanOrEqualTo).GetValue(validationObject, null);

                if (value > compareValue)
                {
                    throw new Exception(
                        string.Format(
                            "The field [{0}][{1}] must be less than or equal to the field [{2}][{3}].",
                            variable,
                            value,
                            rule.lessThanOrEqualTo,
                            compareValue
                        )
                    );
                }
            }
            
            return true;
        }

        private Boolean isValidString(string variable, Rule rule, object validationObject)
        {
            string value = (string) validationObject.GetType().GetProperty(variable).GetValue(validationObject, null);

            if (rule.allowEmpty && String.IsNullOrEmpty(value))
            {
                return true;
            }

            if (rule.maxLength.HasValue && value.Length > rule.maxLength.Value)
            {
                throw new Exception(
                    string.Format(
                        "The field [{0}] has an allowed maximum length of [{1}]. The provided [{0}] has a length of [{2}].",
                        variable,
                        rule.maxLength.Value.ToString(),
                        value.Length
                    )
                );
            }

            if (rule.minLength.HasValue && value.Length < rule.minLength.Value)
            {
                throw new Exception(
                    string.Format(
                        "The field [{0}] has an allowed minimum length of [{1}]. The provided [{0}] has a length of [{2}].",
                        variable,
                        rule.minLength.Value.ToString(),
                        value.Length
                    )
                );
            }

            if (!String.IsNullOrEmpty(rule.validCharacters))
            {
                List <char> invalidCharacters = new List<char>();

                foreach (char c in value)
                {
                    char tmp = c.ToString().ToLower()[0];
                    if (rule.validCharacters.ToLower().IndexOf(tmp) == -1)
                    {
                            invalidCharacters.Add(c);
                    }
                }

                if (invalidCharacters.Count > 0)
                {
                    throw new Exception(
                        string.Format(
                            "The field [{0}] contains invalid characters [{1}].",
                            variable,
                            String.Join(", ", invalidCharacters.ToArray())
                        )
                    );
                }
            }

            return true;
        }
        
        private Boolean isValidSSN(string variable, Rule rule, object validationObject)
        {
            
            string value = (string) validationObject.GetType().GetProperty(variable).GetValue(validationObject, null);

            if (rule.allowEmpty && String.IsNullOrEmpty(value))
            {
                return true;
            }

            Boolean output = false;
            switch (this.configuration["Country"])
            {
                case "NO":
                    output = this.IsValidSSN_Norway(variable, value);
                    break;
                case "SE":
                    output = this.IsValidSSN_Sweden(variable, value);;
                    break;
                case "DK":
                    output = this.IsValidSSN_Denmark(variable, value);;
                    break;
            }

            return output;
        }

        // Checks if the provided SSN is valid for the SSN regulations in Norway
		private Boolean IsValidSSN_Norway(string variable, string value)
        {
            string ValidCharacters = "0123456789";
            int validLength = 11;

            if (value.Length != validLength)
            {
                throw new Exception(
                    string.Format(
                        "The variable [{0}] has an invalid length [{1}]. A valid [{0}] has a length of [{2}] characters.",
                        variable,
                        value.Length.ToString(),
                        validLength.ToString()
                    )
                );
                //return false;
            }

            foreach (char c in value)
            {
                if (!ValidCharacters.Contains(c))
                {
                    throw new Exception(
                        string.Format(
                            "The variable [{0}] contains an invalid character [{1}].",
                            variable,
                            c
                        )
                    );
                    //return false;
                }
            }

            Console.WriteLine(value.Length);

            // Checksum
            int d1 = int.Parse(value.Substring(0, 1));
            int d2 = int.Parse(value.Substring(1, 1));
            int m1 = int.Parse(value.Substring(2, 1));
            int m2 = int.Parse(value.Substring(3, 1));
            int y1 = int.Parse(value.Substring(4, 1));
            int y2 = int.Parse(value.Substring(5, 1));
            int i1 = int.Parse(value.Substring(6, 1));
            int i2 = int.Parse(value.Substring(7, 1));
            int i3 = int.Parse(value.Substring(8, 1));

            int CheckSum1 = 11 - ((3 * d1 + 7 * d2 + 6 * m1 + 1 * m2 + 8 * y1 + 9 * y2 + 4 * i1 + 5 * i2 + 2 * i3) % 11);
            int CheckSum2 = 11 - ((5 * d1 + 4 * d2 + 3 * m1 + 2 * m2 + 7 * y1 + 6 * y2 + 5 * i1 + 4 * i2 + 3 * i3 + 2 * CheckSum1) % 11);

            if (CheckSum1 == 11 && int.Parse(value.Substring(9, 1)) == 0)
            {
                CheckSum1 = 0;
            }

            if (CheckSum2 == 11 && int.Parse(value.Substring(10, 1)) == 0)
            {
                CheckSum2 = 0;
            }

            if (CheckSum1 != int.Parse(value.Substring(9, 1)))
            {
                //throw new Exception("SSN: Invalid checksum 1 [" + CheckSum1.ToString() + " vs. " + value.Substring(9, 1) + "]");
                throw new Exception(
                    string.Format(
                        "The variable [{0}] contains an value [{1}] which results in an invalid checksum].",
                        variable,
                        value
                    )
                );
                //return false;
            }

            if (CheckSum2 != int.Parse(value.Substring(10, 1)))
            {
                //throw new Exception("SSN: Invalid checksum 2 [" + CheckSum2.ToString() + " vs. " + value.Substring(10, 1) + "]");
                    throw new Exception(
                        string.Format(
                            "The variable [{0}] contains an value [{1}] which results in an invalid checksum].",
                            variable,
                            value
                        )
                    );
                //return false;
            }

            return true;
        }

        private Boolean IsValidSSN_Sweden(string variable, string value)
        {
            return true;
        }

        private Boolean IsValidSSN_Denmark(string variable, string value)
        {
            return true;
        }
    }
}