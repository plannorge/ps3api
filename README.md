# Ps3 Signup API

This is a proof of concept API. It was created as alternative to the existing PlanSys API. This API tries to focus on two key points where we feel the current PlanSys API does not meet our requirements.

* Ease of use
* Performance

## Ease of use

In order to be easy to use the client should only need to use terminology relevant to the use case of the client. The client should not need to worry about database internals. Database terminology like NODB, CommitmentId, accountTypeCode, assignmentMode, paymentMethodId, paymentFrequencyCode, commitmentType, categoryCode are not relevant to the client.

In order to be easy to use, the client should not need to chain several calls in order to achieve a use case. One use case = one API call. Ie. CreateSponsor() vs AddExternalRelation(), AddPaymentMethod(), AddCommitment(), AddPreference().

## Performance

We need an API that we can trust will be responsive during heavy load. Occasionally we host events that are broadcasted on national TV, resulting in 5-10k sponsors during a few hours. Any underlying database calls must be non-blocking in order to allow multiple calls to be processed simultaneously. Other blocking operations in PlanSys may affect the API. Preferably blocking operations should be kept to a minimum. TV productions that focuses on recruitment are produced in a way that builds up to certain moments one expect the audience to act. This means that signups are not evenly distributed during the show, peaks at several hundreds per minute should be expected.

## Notes from the developer

This is my first project using .net core in Visual Studio Code.

Bellow are some links used as resources. Maybe they will be helpful to you too:

* https://docs.microsoft.com/en-us/aspnet/core/host-and-deploy/iis/?tabs=aspnetcore2x
* https://auth0.com/blog/securing-asp-dot-net-core-2-applications-with-jwts/

## Testing

### Database
Update the appsettings.json file and adjust the ConnectionStrings.DefaultConnection to reflect your PlanSys database. The user needs read access to the PlanSys database, and in addition the user should have insert rights to the following tables:

* ExternalRelation
* ExternalRelationAddress
* ExternalRelationLanguage
* PersonalId
* ExternalRelationAddressPersonalId
* ExternalRelationPaymentMethod
* Activity
* Activity_ExtensionPS3
* Commitment
* CommitmentAmount
* Commitment_ExtensionPS3
* Preference
* Preference_ExtensionPS3
* PreferenceCountry

### Headers for all requests
All requests should use the following header

    Content-Type: application/json

### Get token and add authorization header
The Ps3Api uses JSON Web Tokens for authentication. To generate a token you need to provide a valid username and password. The username/password combination is checked using LDAP against the configured Active Directory server.

Do a POST request to http://localhost:5000/api/token using the following body (change the username and password to match your user) :

    {
        "username": "mario",
        "password": "secret"
    }

Your response will look something like this:

    {
        "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1MjM0MzMxMjUsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6NjM5MzkvIiwiYXVkIjoiaHR0cDovL2xvY2FsaG9zdDo2MzkzOS8ifQ.8toRviDNF6AeVJBTeujtcRkTkwKiY7BeqBQ6jQ5M1YY"
    }

Copy the token and add a the following header to subsequent requests:

    Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1MjM0MzMxMjUsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3Q6NjM5MzkvIiwiYXVkIjoiaHR0cDovL2xvY2FsaG9zdDo2MzkzOS8ifQ.8toRviDNF6AeVJBTeujtcRkTkwKiY7BeqBQ6jQ5M1YY

### Adding a sponsor
Post a request to http://localhost:5000/api/sponsor/ with the following body:

    {
        "forename": "Terje",
        "surname": "Galematias",
        "street": "Fakestreet",
        "houseNumber": "18",
        "appartmentNumber": "c",
        "postCode": "2100",
        "town": "Skarnes",
        "phoneNumber": "97791051",
        "eMailAddress": "hestnummer3@sludder.no",
        "countryCode": "NOR",
        "dateOfBirth": "1999-11-13",
        "ssn": "",
        "sourceCode": "F2F",
        "preferences":[
            {
                "ageFrom": 4,
                "ageTo": 5,
                "gender": "m",
                "countryCode": "bra",
                "amount": 275
            }
        ]
    }

Adjust the values as you like. For the sake of privacy I have entered bogus data and an empty SSN in the example body.

If your request passed validation and there was no matches in the duplicate check, you should get a HTTP 201 Created response with a body matching the accepted data of your POST request.

## License
Check out the file LICENSE to read more about the GPLv3 license.

## Publish to IIS

Type `dotnet publish -c Releease` in the terminal. Copy the contents of your publish folder `ProjectFolder\bin\Release\netcoreapp2.x\publish` and paste the contents into a folder on your IIS server. Add a new website to IIS and reference the folder you pasted your published files into. Set the application pool to `No Managed Code` .NET CLR version  and `Integrated` managed pipeline mode. I think that's about it, not sure if you need to recyclet he application pool

## Current issues

First request to IIS server after being idle for a while still takes 10 sec +. Subsequent request responds as intended. Is this possible to fix in code? Or is this a IIS confiugration issue?

The following resource seems to help quite a lot, but still the first request takes 10 times of subsequent requests.

## Documentation

This project uses Swagger. You should be abel to visit the base address of the site where this API is hosted and get an overview over possible HTTP verbs, URLs and models.


## TODO

* Let existing sponsors add preferenes