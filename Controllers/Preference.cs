using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authorization;
using Ps3Api.Models;

namespace Ps3Api.Controllers
{
    [Route("api/[controller]")]
    public class PreferenceController : Controller
    {
        private IConfiguration configuration;

        public PreferenceController(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        // GET api/values/5
        [HttpGet("{id}", Name = "GetPreference"), Authorize]
        public IActionResult Get(Int64 id)
        {
			Ps3Api.Misc.PlanSys planSys = new Ps3Api.Misc.PlanSys(configuration);

			return new ObjectResult(planSys.GetPreference(id));
        }

        // POST api/values
        [HttpPost, Authorize]
        public IActionResult Post([FromBody]PreferenceWithSpNumber preference)
        {
            Int64 preferenceId = -1;
			Error error = null;

            Console.WriteLine("Hello today!");
            Console.WriteLine("gender: [" + preference.preference.gender + "]");
            Console.WriteLine("countryCode: [" + preference.preference.countryCode + "]");
            Console.WriteLine("ageFrom: [" + preference.preference.ageFrom + "]");
            Console.WriteLine("ageTo: [" + preference.preference.ageTo + "]");
            Console.WriteLine("amount: [" + preference.preference.ageTo + "]");
            Console.WriteLine("spNumber: [" + preference.spNumber + "]");

            Ps3Api.Misc.PlanSys planSys = new Ps3Api.Misc.PlanSys(configuration);;
			Ps3Api.Misc.Validation validation = new Ps3Api.Misc.Validation(configuration);

			try {
				validation.isValid("preference", preference.preference);
			} catch (Exception exception)
			{
					Console.WriteLine(exception.Message);
					error = new Error(400, exception.Message);
					return BadRequest(error);
			}

            Sponsor sponsor = planSys.GetExternalRelation(Convert.ToInt64(preference.spNumber));

            if (sponsor == null)
            {
					error = new Error(400, "Invalid SPNumber");
					return BadRequest(error);
            }

            try {
                preferenceId = planSys.CreatePreference(sponsor, preference.spNumber, preference.preference);
            } catch (Exception exception) {
                Console.WriteLine(exception.Message);
				error = new Error(500, exception.Message);
				return StatusCode(500, error);
            }

			return CreatedAtRoute("GetPreference", new {id = preferenceId}, planSys.GetPreference(preferenceId));
        }

        /*
        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
            // TODO: Update data and insert teh data into the relevant database tables in PS
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            // TODO: Not realy sure if we need this one. Look into it later
        }
        */
    }
}
