using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authorization;
using Ps3Api.Models;

namespace Ps3Api.Controllers
{
    [Route("api/[controller]")]
    public class DualSponsorController : Controller
    {
        private IConfiguration configuration;
        //private System.Data.SqlClient.SqlConnection connection;

        public DualSponsorController(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        /*
        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            // One should not be able to get all sponsors. I think this function should be removed.
            return new string[] { "value1", "value2" };
        }
         */

        // GET api/values/5
        [HttpGet("{id}", Name = "GetDualSponsor"), Authorize]
        public IActionResult Get(Int64 id)
        {
			Ps3Api.Misc.PlanSys planSys = new Ps3Api.Misc.PlanSys(configuration);

			return new ObjectResult(planSys.GetExternalRelation(id));
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody]DualSponsor dualSponsor)
        {
            Int64 externalRelationNumberPrimary = -1;
            //Int64 externalRelationNumberSecondary = -1;
			Error error = null;

            Ps3Api.Misc.PlanSys planSys = new Ps3Api.Misc.PlanSys(configuration);;
			Ps3Api.Misc.Validation validation = new Ps3Api.Misc.Validation(configuration);

            // TODO: right now there is a sourceCode for both the Primary and the Secondary sponsor. This is not neccessary. Maybe change this? It is very convenient for the API developer the way it currently is.

			try {
				validation.isValid("sponsor", dualSponsor.primarySponsor);
			} catch (Exception exception)
			{
					Console.WriteLine(exception.Message);
					error = new Error(400, exception.Message);
					return BadRequest(error);
			}

            try {
				validation.isValid("sponsor", dualSponsor.secondarySponsor);
			} catch (Exception exception)
			{
					Console.WriteLine(exception.Message);
					error = new Error(400, exception.Message);
					return BadRequest(error);
			}

			foreach (Preference preference in dualSponsor.preferences)
			{
				try {
					validation.isValid("preference", preference);
				} catch (Exception exception)
				{
						Console.WriteLine(exception.Message);
						error = new Error(400, exception.Message);
						return BadRequest(error);
				}

			}

            Dictionary<Int64, string> duplicates = planSys.GetDuplicates(dualSponsor.primarySponsor);

            if (duplicates.Count > 0)
            {
                string message = string.Format(
                    "The duplicate check showed that one or more accounts with the provided data already exists. {0}",
                    String.Join(", ", duplicates.ToArray())
                );
				Console.WriteLine(message);
				error = new Error(409, message);
				return StatusCode(409, error);
            }

            duplicates = planSys.GetDuplicates(dualSponsor.secondarySponsor);

            if (duplicates.Count > 0)
            {
                string message = string.Format(
                    "The duplicate check showed that one or more accounts with the provided data already exists. {0}",
                    String.Join(", ", duplicates.ToArray())
                );
				Console.WriteLine(message);
				error = new Error(409, message);
				return StatusCode(409, error);
            }

            // Attaching the preference to he primary sponsor. By doing this we can reuse existing planSys functionality.
            dualSponsor.primarySponsor.preferences = dualSponsor.preferences;

            /*
            try {
                externalRelationNumberPrimary = planSys.CreateExternalRelation(dualSponsor.primarySponsor);
            } catch (Exception exception) {
                Console.WriteLine(exception.Message);
				error = new Error(500, exception.Message);
				return StatusCode(500, error);
            }
            */

            /* TODO: I am not sure we can reuse the planSys.CreateExternalRelation() function. The current function adds both ExternalRelationPaymentMethod and Activity, we need to check with DNO in order to get it right.
            try {
                externalRelationNumberSecondary = planSys.CreateExternalRelation(dualSponsor.secondarySponsor);
            } catch (Exception exception) {
                Console.WriteLine(exception.Message);
				error = new Error(500, exception.Message);
				return StatusCode(500, error);
            }
            */

            // TODO: We need to add an ExternalRelationLink
            /*
            try {
                planSys.LinkExternalRelations(externalRelationNumberPrimary, externalRelationNumberSecondary);
            } catch (Exception exception) {
                Console.WriteLine(exception.Message);
				error = new Error(500, exception.Message);
				return StatusCode(500, error);
            }
            */

			return CreatedAtRoute("GetDualSponsor", new {id = externalRelationNumberPrimary}, dualSponsor);

        }

        /*
        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
            // TODO: Update data and insert teh data into the relevant database tables in PS
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            // TODO: Not realy sure if we need this one. Look into it later
        }
        */
    }
}
