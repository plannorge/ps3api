using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Ps3Api.Controllers
{
	[Route("api/[controller]")]
	public class TokenController : Controller
	{
		private IConfiguration configuration;

		public TokenController(IConfiguration configuration)
		{
			this.configuration = configuration;
		}

		[AllowAnonymous]
		[HttpPost]
		public IActionResult CreateToken([FromBody]LoginModel login)
		{
			IActionResult response = Unauthorized();
			var user = this.Authenticate(login);

			if (user != null)
			{
				var tokenString = BuildToken(user);
				response = Ok(new { token = tokenString });
			}

			return response;
		}

		private string BuildToken(UserModel user)
		{
			var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(this.configuration["Jwt:Key"]));
			var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

			var token = new JwtSecurityToken(
				this.configuration["Jwt:Issuer"],
				this.configuration["Jwt:Issuer"],
				expires: DateTime.Now.AddMinutes(30),
				signingCredentials: creds
			);

			return new JwtSecurityTokenHandler().WriteToken(token);
		}

		private UserModel Authenticate(LoginModel login)
		{
			UserModel user = null;

			Novell.Directory.Ldap.LdapEntry searchEntry;

			Ps3Api.Misc.LDAP ldap = new Ps3Api.Misc.LDAP(
				this.configuration["LDAP:Server"],
				Int32.Parse(this.configuration["LDAP:Port"]),
				Int32.Parse(this.configuration["LDAP:Timelimit"]),
				this.configuration["LDAP:Domain"],
				this.configuration["LDAP:Suffix"]
			);

			try{
				ldap.Connect();
			}
			catch (Exception exception)
			{
				Console.WriteLine(exception.Message);
				return user;
			}

			try {
				ldap.Bind(login.Username, login.Password);
			}
			catch (Exception exception)
			{
				Console.WriteLine(exception.Message);
				return user;
			}

			searchEntry = ldap.SearchAccountName(login.Username);

			if (searchEntry == null)
			{
				return user;
			}

			if (ldap.IsEntryMemberOf(searchEntry, this.configuration["LDAP:APIGroup"]))
			{
					Novell.Directory.Ldap.LdapAttribute name = searchEntry.getAttribute("name");
					Novell.Directory.Ldap.LdapAttribute mail = searchEntry.getAttribute("mail");

					user = new UserModel { Name = name.StringValue, Email = mail.StringValue};
			}

			ldap.Close();

			return user;
		}

		public class LoginModel
		{
			public string Username { get; set; }
			public string Password { get; set; }
		}

		private class UserModel
		{
			public string Name { get; set; }
			public string Email { get; set; }
		}
	}
}