using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authorization;
using Ps3Api.Models;

namespace Ps3Api.Controllers
{
    [Route("api/[controller]")]
    public class SponsorController : Controller
    {
        private IConfiguration configuration;

        public SponsorController(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        // GET api/values/5
        [HttpGet("{id}", Name = "GetSponsor"), Authorize]
        //public IActionResult Get(Int64 id)
        public ActionResult<Sponsor> Get(Int64 id)
        {
			Ps3Api.Misc.PlanSys planSys = new Ps3Api.Misc.PlanSys(configuration);

			//return new ObjectResult(planSys.GetExternalRelation(id));
            return planSys.GetExternalRelation(id);
        }

        // POST api/values
        [HttpPost, Authorize]
        public IActionResult Post([FromBody]Sponsor sponsor)
        {
            Int64 externalRelationNumber = -1;
			Error error = null;

            Ps3Api.Misc.PlanSys planSys = new Ps3Api.Misc.PlanSys(configuration);;
			Ps3Api.Misc.Validation validation = new Ps3Api.Misc.Validation(configuration);

			try {
				validation.isValid("sponsor", sponsor);
			} catch (Exception exception)
			{
					Console.WriteLine(exception.Message);
					error = new Error(400, exception.Message);
					return BadRequest(error);
			}

			foreach (Preference preference in sponsor.preferences)
			{
				try {
					validation.isValid("preference", preference);
				} catch (Exception exception)
				{
						Console.WriteLine(exception.Message);
						error = new Error(400, exception.Message);
						return BadRequest(error);
				}

			}

            Dictionary<Int64, string> duplicates = planSys.GetDuplicates(sponsor);

            if (duplicates.Count > 0)
            {
                string message = string.Format(
                    "The duplicate check showed that one or more accounts with the provided data already exists. {0}",
                    String.Join(", ", duplicates.ToArray())
                );
				Console.WriteLine(message);
				error = new Error(409, message);
				return StatusCode(409, error);
            }

            try {
                externalRelationNumber = planSys.CreateExternalRelation(sponsor);
            } catch (Exception exception) {
                Console.WriteLine(exception.Message);
				error = new Error(500, exception.Message);
				return StatusCode(500, error);
            }

            Request.HttpContext.Response.Headers.Add("id", externalRelationNumber.ToString());

			return CreatedAtRoute("GetSponsor", new {id = externalRelationNumber}, planSys.GetExternalRelation(externalRelationNumber));
        }

        /*
        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
            // TODO: Update data and insert teh data into the relevant database tables in PS
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            // TODO: Not realy sure if we need this one. Look into it later
        }
        */
    }
}
